package com.worksystem.Service;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.worksystem.Dao.Dao;
import com.worksystem.entity.PageBean;

public class BaseService<E> implements Service<E> {

	protected Dao<E> dao;
	public Dao<E> getDao() {
		return dao;
	}
	public void setDao(Dao<E> dao) {
		this.dao = dao;
	}

	public boolean delete(E entity){
		return dao.delete(entity);
	}
	@Override
	public boolean deleteByPK(Serializable... id) {
		return dao.deleteByPK(id);
	}

	@Override
	public boolean save(E entity) {
		return dao.save(entity);
	}

	@Override
	public void deleteByProperties(String propName, Object propValue) {
		dao.deleteByProperties(propName, propValue);
	}

	@Override
	public void deleteByProperties(String[] propName, Object[] propValue) {
		dao.deleteByProperties(propName, propValue);
	}

	@Override
	public void update(E entity) {
		dao.update(entity);
	}

	@Override
	public void updateByProperties(String[] conditionName,
			Object[] conditionValue, String[] propertyName,
			Object[] propertyValue) {
		dao.updateByProperties(conditionName, conditionValue, propertyName, propertyValue);
	}

	@Override
	public void updateByProperties(String[] conditionName,
			Object[] conditionValue, String propertyName, Object propertyValue) {
		dao.updateByProperties(conditionName, conditionValue, propertyName, propertyValue);
	}

	@Override
	public void updateByProperties(String conditionName, Object conditionValue,
			String[] propertyName, Object[] propertyValue) {
		dao.updateByProperties(conditionName, conditionValue, propertyName, propertyValue);
	}

	@Override
	public void updateByProperties(String conditionName, Object conditionValue,
			String propertyName, Object propertyValue) {
		dao.updateByProperties(conditionName, conditionValue, propertyName, propertyValue);
		
	}

	@Override
	public void update(E entity, Serializable oldId) {
		dao.update(entity, oldId);
	}

	@Override
	public E getByProerties(String[] propName, Object[] propValue) {
		return  dao.getByProerties(propName, propValue);
	}

	@Override
	public E getByProerties(String[] propName, Object[] propValue,
			Map<String, String> sortedCondition) {
		return getByProerties(propName, propValue, sortedCondition);
	}

	@Override
	public E getByProerties(String propName, Object propValue) {
		return  dao.getByProerties(propName, propValue);
	}

	@Override
	public E getByProerties(String propName, Object propValue,
			Map<String, String> sortedCondition) {
		return  dao.getByProerties(propName, propValue, sortedCondition);
	}

	@Override
	public List<E> queryByProerties(String[] propName, Object[] propValue,
			Map<String, String> sortedCondition, Integer top) {
		return dao.queryByProerties(propName, propValue,sortedCondition, top);
	}

	@Override
	public List<E> queryByProerties(String[] propName, Object[] propValue,
			Map<String, String> sortedCondition) {
		return dao.queryByProerties(propName, propValue,sortedCondition);
	}

	@Override
	public List<E> queryByProerties(String[] propName, Object[] propValue,
			Integer top) {
		return dao.queryByProerties(propName, propValue, top);
	}

	@Override
	public List<E> queryByProerties(String[] propName, Object[] propValue) {
		return dao.queryByProerties(propName, propValue);
	}

	@Override
	public List<E> queryByProerties(String propName, Object propValue,
			Map<String, String> sortedCondition, Integer top) {
		return dao.queryByProerties(propName, propValue,sortedCondition, top);
	}

	@Override
	public List<E> queryByProerties(String propName, Object propValue,
			Map<String, String> sortedCondition) {
		return dao.queryByProerties(propName, propValue,sortedCondition);
	}

	@Override
	public List<E> queryByProerties(String propName, Object propValue,
			Integer top) {
		return dao.queryByProerties(propName, propValue, top);
	}

	@Override
	public List<E> queryByProerties(String propName, Object propValue) {
		return dao.queryByProerties(propName, propValue);
	}

	@Override
	public Long countAll() {
		return dao.countAll();
	}
	public Long countAll(String propName, Object propValue){
		return dao.countAll(propName, propValue);
	}
	public Long countAll(String[] propName, Object[] propValue){
		return dao.countAll(propName, propValue);
	}
	@Override
	public List<E> doQueryAll() {
		return dao.doQueryAll();
	}

	@Override
	public List<E> doQueryAll(Map<String, String> sortedCondition, Integer top) {
		return dao.doQueryAll(sortedCondition, top);
	}

	@Override
	public List<E> doQueryAll(Integer top) {
		return dao.doQueryAll(top);
	}

	@Override
	public PageBean doPaginationQuery(@SuppressWarnings("rawtypes") List entitys, int page) {
		return dao.doPaginationQuery(entitys, page);
	}

	public PageBean initPageBean(Integer pageId,Long rowscount){		
		return dao.initPageBean(pageId, rowscount);
	}
}
