package com.worksystem.Service;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.worksystem.Dao.CommonDao;

@Component
public class CommonServiceImp implements CommonService{
	
	@Resource
	private CommonDao commonDao;

	@Override
	public boolean createdata() {
		return commonDao.createdata();
	}
	
}
