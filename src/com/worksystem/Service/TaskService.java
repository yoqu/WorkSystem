package com.worksystem.Service;

import java.util.List;

import com.worksystem.entity.Comment;
import com.worksystem.entity.PageBean;
import com.worksystem.entity.Subject;
import com.worksystem.entity.Task;
import com.worksystem.entity.User;

public interface TaskService extends Service<Task>{
	
	/**
	 * 根据用户的角色来获取任务列表
	 * @param user 用户实体
	 * @param start 开始位置
	 * @param length 要取的长度
	 * @return 返回任务集合
	 */
	public PageBean getUserTasklist(User user,Integer pageid);
	
	public User getUserByTaskId(int tid);
	
	public Task getTaskById(Integer id);
	
	public List<Comment> getCommentsByTaskId(Integer id);
	public void saveComment(Comment comment);
	
	public Integer Inserttask(String title,Integer uid,String time,String body,Object[] uids,Object[] subjectids);
	
	public Integer deletetask(Integer tid,Integer uid);
	
	public Integer updatetask(Integer tid,String title,Integer uid,String time,String body,Object[] uids);
	
	public List<Subject> getTosByTaskId(Integer tid);
	
	public List<Subject> getSubjectsByClassId(Integer classId);
}
