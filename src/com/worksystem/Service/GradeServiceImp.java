package com.worksystem.Service;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.worksystem.Dao.GradeDao;
import com.worksystem.entity.Grade;

@Component
public class GradeServiceImp extends BaseService<Grade> implements GradeService {
	private GradeDao gradeDao;
	public GradeDao getGradeDao() {
		return gradeDao;
	}
	@Resource
	public void setGradeDao(GradeDao gradeDao) {
		this.gradeDao = gradeDao;
		this.dao=gradeDao;
	}
}
