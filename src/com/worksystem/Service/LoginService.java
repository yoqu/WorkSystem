package com.worksystem.Service;

import java.util.List;

import com.worksystem.entity.Classes;
import com.worksystem.entity.Department;
import com.worksystem.entity.Student;
import com.worksystem.entity.Teacher;
import com.worksystem.entity.User;

public interface LoginService extends Service<User>{
	public boolean dologin(User user);
	public User getUserByLoginId(String LoginId);
	public boolean doregister(User user);
	public User getUserByUid(Integer uid);
	public List<Classes> getClassesbyDid(Integer did);
	public Classes getClassesById(Integer cid);
	public List<Department> getAllDepartment();
	public List<Classes> getAllClasses();
	
	public int doregisterstudent(User user,Student student);
	public int doregisterteacher(User user ,Teacher teacher);
	
	public List<User> getStudentUsersByClasses(Integer cid,Integer start,Integer length);
	
	public Student getStudentByUid(Integer uid);
	
	public Teacher getTeacherByUid(Integer uid);
	public List<User> getPokermanUserByTask(Integer tid);
	
	public boolean updateprofile(User user);
	public List<Student> getStudentsByClasses(Integer cid);
}
