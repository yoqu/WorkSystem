package com.worksystem.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.worksystem.Dao.LoginDao;
import com.worksystem.Dao.TaskDao;
import com.worksystem.entity.Comment;
import com.worksystem.entity.PageBean;
import com.worksystem.entity.Subject;
import com.worksystem.entity.Task;
import com.worksystem.entity.Teacher;
import com.worksystem.entity.User;

@Component("taskService")
public class TaskServiceImp extends BaseService<Task> implements TaskService{
	
	
	private TaskDao taskDao;
	public TaskDao getTaskDao() {
		return taskDao;
	}
	@Resource(name="taskDao")
	public void setTaskDao(TaskDao taskDao) {
		this.taskDao = taskDao;
		this.dao=taskDao;
	}
	@Resource
	private LoginDao userDao;
	
	@Override
	public PageBean getUserTasklist(User user,Integer pageid) {
		if(user.getRole().getRid()==1){
			System.out.println("执行老师方法了");
			return taskDao.getTeacherTasklist(user,pageid);
		}
		else if(user.getRole().getRid()==2){
			System.out.println("执行嗯学生方法");
			return taskDao.getStudentTasklist(user, pageid);
		}
		else{
			return null;
		}
	}
	@Override
	public User getUserByTaskId(int tid) {
		return taskDao.getUserByTaskId(tid);
	}
	@Override
	public Task getTaskById(Integer id) {
		return taskDao.getTaskById(id);
	}
	@Override
	public List<Comment> getCommentsByTaskId(Integer id) {
		return taskDao.getCommentsByTaskId(id);
	}
	@Override
	public void saveComment(Comment comment) {
		 
		taskDao.saveComment(comment);
	}
	
	@Override
	public Integer Inserttask(String title,Integer uid,String time,String body,Object[] uids,Object[] subjectids){
		//保存任务
		Task task = new Task();
		try {
			Teacher teacher =userDao.getTeacherByUid(uid);
			task.setClassesid(teacher.getClasses().getCid());
		} catch (Exception e) {
			//如果uid不是学生的话抛出异常。
			System.out.println("教师查询异常");
			return 102;
		}
		User user=userDao.getUserByUid(uid);
		task.setBody(body);
		task.setComment(0);
		SimpleDateFormat sdf  =   new  SimpleDateFormat( "yyyy-MM-dd HH:mm" ); 
		try {
			Date limittime=sdf.parse(time);
			task.setLimittime(limittime);
			task.setPokerman(uids.length);
			task.setStatus(1);
			task.setTime(new Date());
			task.setTitle(title);
			task.setUser(user);
		} catch (Exception e) {
			System.out.println("日期出问题了，抛出错误代码101");
			return 101;
		}
		return taskDao.inserttask(task,uids,subjectids);
		
	}
	
	public Integer deletetask(Integer tid,Integer uid){
		//----权限检查---
		System.out.println("开始删除任务");
		return taskDao.deletetask(tid);
	}
	@Override
	public Integer updatetask(Integer tid,String title,Integer uid,String time,String body,Object[] uids){
		//保存任务
				Task task = taskDao.getTaskById(tid);
				try {
					Teacher teacher =userDao.getTeacherByUid(uid);
					task.setClassesid(teacher.getClasses().getCid());
				} catch (Exception e) {
					//如果uid不是学生的话抛出异常。
					System.out.println("教师查询异常");
					return 102;
				}
				User user=userDao.getUserByUid(uid);
				task.setBody(body);
				SimpleDateFormat sdf  =   new  SimpleDateFormat( "yyyy-MM-dd HH:mm" ); 
				try {
					Date limittime=sdf.parse(time);
					task.setLimittime(limittime);
					task.setPokerman(uids.length);
					//task.setStatus(1);
					task.setTime(new Date());
					task.setTitle(title);
					task.setUser(user);
				} catch (Exception e) {
					System.out.println("日期出问题了，抛出错误代码101");
					return 101;
				}
				return taskDao.updatetask(task, uids);
	}
	@Override
	public List<Subject> getTosByTaskId(Integer tid){
		return taskDao.getTosByTaskId(tid);
	}
	public List<Subject> getSubjectsByClassId(Integer classId){
		return taskDao.getSubjectsByClassId(classId);
	}
}
