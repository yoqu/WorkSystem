package com.worksystem.Service;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.worksystem.Dao.AnswerDao;
import com.worksystem.entity.Answer;

@Component
public class AnswerServiceImp extends BaseService<Answer> implements AnswerService{

	private AnswerDao answerDao;

	public AnswerDao getAnswerDao() {
		return answerDao;
	}
	@Resource
	public void setAnswerDao(AnswerDao answerDao) {
		this.answerDao = answerDao;
		this.dao=answerDao;
	}
	
}
