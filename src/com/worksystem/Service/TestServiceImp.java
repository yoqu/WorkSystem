package com.worksystem.Service;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.worksystem.Dao.TestDao;
import com.worksystem.entity.Test;

@Component("testService")
public class TestServiceImp extends BaseService<Test> implements TestService {
	
	
	private TestDao testDao;

	public TestDao getTestDao() {
		return testDao;
	}
	@Resource
	public void setTestDao(TestDao testDao) {
		this.testDao = testDao;
		this.dao=testDao;
	}

	@Override
	public Object postteststograde(String testjson,Integer uid,Integer subjectid,String time){
		return testDao.postteststograde(testjson, uid, subjectid, time);
	}
	
}
