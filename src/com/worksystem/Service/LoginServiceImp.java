package com.worksystem.Service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.worksystem.Dao.BaseDao;
import com.worksystem.Dao.LoginDao;
import com.worksystem.entity.Classes;
import com.worksystem.entity.Department;
import com.worksystem.entity.Student;
import com.worksystem.entity.Teacher;
import com.worksystem.entity.User;

@Component
public class LoginServiceImp extends BaseDao<User> implements LoginService{
	
	public LoginServiceImp() {
		super(User.class);
	}

	@Resource
	private LoginDao loginDaoimp;
	
	@Override
	public boolean dologin(User user) {
		return loginDaoimp.dologin(user);
	}
	
	@Override
	public User getUserByLoginId(String LoginId){
		return loginDaoimp.getUserByLoginId(LoginId);
	}

	@Override
	public boolean doregister(User user) {
		return loginDaoimp.doregister(user);
	}
	@Override
	public User getUserByUid(Integer uid){
		return loginDaoimp.getUserByUid(uid);
	}

	@Override
	public List<Classes> getClassesbyDid(Integer did) {
		return loginDaoimp.getClassesbyDid(did);
	}

	@Override
	public Classes getClassesById(Integer cid) {
		return loginDaoimp.getClassesById(cid);
	}

	@Override
	public List<Department> getAllDepartment() {
		return loginDaoimp.getAllDepartment();
	}

	@Override
	public List<Classes> getAllClasses() {
		return loginDaoimp.getAllClasses();
	}

	@Override
	public int doregisterstudent(User user, Student student) {
		return loginDaoimp.doregisterstudent(user, student);
	}

	@Override
	public int doregisterteacher(User user, Teacher teacher) {
		return loginDaoimp.doregisterteacher(user, teacher);
	}

	@Override
	public List<User> getStudentUsersByClasses(Integer cid, Integer start, Integer length) {
		return loginDaoimp.getStudentUsersByClasses(cid, start, length);
	}
	
	
	@Override
	public Student getStudentByUid(Integer uid){
		return loginDaoimp.getStudentByUid(uid);
	}
	
	@Override
	public Teacher getTeacherByUid(Integer uid){
		return loginDaoimp.getTeacherByUid(uid);
	}
	@Override
	public List<User> getPokermanUserByTask(Integer tid){
		return loginDaoimp.getPokermanUserByTask(tid);
	}
	public boolean updateprofile(User user){
		return loginDaoimp.updateprofile(user);
	}
	
	public List<Student> getStudentsByClasses(Integer cid){
		return loginDaoimp.getStudentsByClasses(cid);
	}
}
