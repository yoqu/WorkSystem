package com.worksystem.Service;

import javax.annotation.Resource;
import org.springframework.stereotype.Component;
import com.worksystem.Dao.SubjectDao;
import com.worksystem.entity.Subject;
@Component
public class SubjectServiceImp extends BaseService<Subject> implements SubjectService{
	
	private SubjectDao subjectDao;
	public SubjectDao getSubjectDao() {
		return subjectDao;
	}
	
	@Resource
	public void setSubjectDao(SubjectDao subjectDao) {
		this.subjectDao = subjectDao;
		this.dao=subjectDao;
	}
	
}
