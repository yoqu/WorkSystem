package com.worksystem.Controller;


import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.worksystem.Common.Common;
import com.worksystem.Common.Constant;
import com.worksystem.Service.LoginService;
import com.worksystem.entity.*;

import net.sf.json.JSONArray;

@Controller
@RequestMapping(value="/user")
public class UserController extends Common{
	@Resource
	private LoginService logindao;
	@RequestMapping()
	public ModelAndView defaultmethod (HttpServletRequest request, HttpServletResponse response)  {
		HttpSession session=request.getSession();
		if(session.getAttribute("onlineuser")!=null){
			return userprofile();
		}
		else{
			return login();
		}
	}
	
	@RequestMapping("/profile")
	public ModelAndView userprofile(){
		HttpSession session=request.getSession();
		if(session.getAttribute(Constant.SESSION_USER)==null){
			return login();
		}
		ModelAndView mav= new ModelAndView();
		User user = (User) request.getSession().getAttribute(Constant.SESSION_USER);
		mav.addObject(user);
		mav.setViewName("/user/profile");
		return mav;
	}
	//登陆页面
	@RequestMapping("/login")
	public ModelAndView login()  {
		ModelAndView mv=new ModelAndView();
		mv.setViewName("/user/login");
		return mv;
	}
	
	@RequestMapping("/register")
	public ModelAndView register()  {
		ModelAndView mv=new ModelAndView();
		mv.setViewName("/user/register");
		return mv;
	}
	
	//登陆操作
	@RequestMapping(value = "/dologin", method = { RequestMethod.POST, RequestMethod.GET },produces = "text/html;charset=UTF-8")
	@ResponseBody
	public Object dologin(User user){
		if(logindao.dologin(user)){
		System.out.println("登陆成功");
		User nowUser=logindao.getUserByLoginId(user.getLoginId());
		//获得session并将当前得到的用户信息封到session里面	
		HttpSession session=request.getSession();
			session.setAttribute("onlineuser",nowUser);
			initDefaultModel();
			JSONArray ja=processorParameter(nowUser, new String[]{"loginPwd"});
			return ja.toString();
		}
		else{
			return "false";
		}
	}
	//学生注册
		@RequestMapping(value = "/doregisterstudent", method = { RequestMethod.POST, RequestMethod.GET },produces = "text/html;charset=UTF-8")
		@ResponseBody
	public Object doregisterstudent(User user,String name,Integer age,Integer department,Integer classes){
			user.setCreated(new Date());
			//设置它的权限为学生
			Roles roles= new Roles();
			roles.setRid(2);
			user.setRole(roles);
			Student student=new Student();
			student.setAge(age);
			Classes classesentity=new Classes();
			Department dm=new Department();
			dm.setDid(department);
			classesentity.setCid(classes);
			student.setClasses(classesentity);
			student.setDepartment(dm);
			return String.valueOf(logindao.doregisterstudent(user, student));
	}
	
		//教师注册
		@RequestMapping(value = "/doregisterteacher", method = { RequestMethod.POST, RequestMethod.GET },produces = "text/html;charset=UTF-8")
		@ResponseBody
	public Object doregisterteacher(User user,String name,Integer age,Integer department,Integer classes,String teatitle){
			user.setCreated(new Date());
			//设置权限为教师
			Roles roles= new Roles();
			roles.setRid(1);
			user.setRole(roles);
			System.out.println("用户Id"+user.getLoginId());
			System.out.println("用户密码"+user.getLoginPwd());
			Teacher teacher=new Teacher();
			teacher.setAge(age);
			Classes classesentity=new Classes();
			Department dm=new Department();
			dm.setDid(department);
			classesentity.setCid(classes);
			teacher.setClasses(classesentity);
			teacher.setDepartment(dm);
			teacher.setTeatitle(teatitle);
			return String.valueOf(logindao.doregisterteacher(user, teacher));
	}
		
		
	//教师注册
		@RequestMapping(value = "/dochangeprofile", method = { RequestMethod.POST, RequestMethod.GET },produces = "text/html;charset=UTF-8")
		@ResponseBody
		public Object dochangeprofile(String name,String oldpwd,String newpwd,String againpwd){
			HttpSession session=request.getSession();
			if(session.getAttribute("onlineuser")!=null){
				User user = (User) request.getSession().getAttribute(Constant.SESSION_USER);
				user.setName(name);
				if(!oldpwd.equals(user.getLoginPwd())){
					return "108";
				}
				if(!newpwd.equals(againpwd)){
					return "109";
				}
				user.setLoginPwd(newpwd);
				if(logindao.updateprofile(user)){
					session.removeAttribute(Constant.SESSION_USER);
					return "1";
				}else{
					return "0";
				}
			}
			else{
				return "0";
			}
		}
		
	//退出登录
		@RequestMapping("/logout")
		public ModelAndView logout(){
			HttpSession session=request.getSession();
			if(session.getAttribute("onlineuser")!=null){
				session.removeAttribute("onlineuser");
			}
			//清空模块视图
			if(!mav.isEmpty()){
				mav.clear();
			}
			mav.setViewName("/user/login");
			return mav;
		}
		
		
		@RequestMapping(value="/ajax/getDepartments",method = { RequestMethod.POST, RequestMethod.GET },produces = "text/html;charset=UTF-8")
		@ResponseBody
		public Object getDepartmentsByJSON(){
			List<Department> departments= logindao.getAllDepartment();
			List<Classes> classes=logindao.getAllClasses();
			if(departments.size()>0 && classes.size()>0){
				JSONArray djson=processorParameter(departments, new String[]{"students","classeses"});
				JSONArray cjson=processorParameter(classes, new String[]{"students","classeses"});
				JSONObject ob=new JSONObject();
				ob.put("department", djson);
				ob.put("classes", cjson);
				return ob.toString();
			}
			else{
				return "false";
			}
			
		}	
		@RequestMapping(value="/ajax/getClasses",method = { RequestMethod.POST, RequestMethod.GET },produces = "text/html;charset=UTF-8")
		@ResponseBody
		public Object getClassesByJSON(Integer did){
			if(did!=null){
				List<Classes> classes=logindao.getClassesbyDid(did);
				if(classes.size()>0){
					JSONArray ja=processorParameter(classes, new String[]{"students","classeses","department"});
					return ja.toString();
				}
				else{
					return "false";
				}
			}
			else{
				return "false";
			}
			
		}	
	
}
