package com.worksystem.Controller;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;

import org.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.worksystem.Common.Common;
import com.worksystem.Common.Constant;
import com.worksystem.Service.LoginService;
import com.worksystem.Service.TaskService;
import com.worksystem.entity.Comment;
import com.worksystem.entity.PageBean;
import com.worksystem.entity.Subject;
import com.worksystem.entity.Task;
import com.worksystem.entity.User;
import com.worksystem.syscontroller.ManageTaskController;

@Controller
@RequestMapping("dashboard")
public class DashboardController extends Common {
	
	@Resource(name="taskService")
	private TaskService taskService;
	
	@Resource
	private ManageTaskController taskController; 
	@Resource
	private LoginService UserDao;
	//进入默认主页
	@RequestMapping()
	public ModelAndView defaultmethod(HttpServletRequest request, HttpServletResponse response)  {
		ModelAndView mv=new ModelAndView();
		HttpSession session=request.getSession();
			User user=(User) session.getAttribute(Constant.SESSION_USER);
			//判断是老师还是学生，老师进入任务管理，学生进入任务查看
			if(user.getRole().getRid()==1)//教师
			{
				return taskController.tasklist();
			}
			mv.setViewName("/dashboard/dashboard");
			if(request.getParameter("page")!=null){
				int page = Integer.parseInt(request.getParameter("page").toString())+1;
				System.out.println(page);
				PageBean pagebean =taskService.getUserTasklist(user, page);
				mv.addObject("pagebean", pagebean);
			}else{
				PageBean pagebean = taskService.getUserTasklist(user, 1);
				mv.addObject("pagebean", pagebean);
			}
			mv.addObject("user",user);
		return mv;
	}
	
	@RequestMapping(value="/ajax/list",method = { RequestMethod.POST, RequestMethod.GET },produces = "text/html;charset=UTF-8")
	@ResponseBody
	public Object getTaskByJSON(Integer uid){
		if(uid!=null){
			User user=UserDao.getUserByUid(uid);
			PageBean pagebean = taskService.getUserTasklist(user, 1);
			JSONArray ja=processorParameter(pagebean.getList(), new String[]{"loginPwd"});
			return ja.toString();
		}
		else{
			HttpSession session=request.getSession();
				User user=(User) session.getAttribute(Constant.SESSION_USER);
				PageBean pagebean = taskService.getUserTasklist(user, 1);
				JSONArray ja=processorParameter(pagebean.getList(), new String[]{"loginPwd"});
				return ja.toString();
		}
		
		
	}
	
	@RequestMapping(value="/ajax/taskuserlist",method = { RequestMethod.POST, RequestMethod.GET },produces = "text/html;charset=UTF-8")
	@ResponseBody
	public Object getTaskuserlistByJSON(Integer tid){
		if(tid!=null){
			User user=taskService.getUserByTaskId(tid);
			JSONObject ob=new JSONObject(user);
			return ob.toString();
		}
		else{
			return "false";
		}
	}
	
	@RequestMapping(value="/ajax/taskdetail",method = { RequestMethod.POST, RequestMethod.GET },produces = "text/html;charset=UTF-8")
	@ResponseBody
	public Object getTaskinfomation(Integer tid){
		if(tid==null){
			return false;
		}
		Task task=taskService.getTaskById(tid);
		if(task!=null){
			List<Comment> comments=taskService.getCommentsByTaskId(tid);
			JSONArray taskjson=processorParameter(task,null);
			JSONArray commentjson=processorParameter(comments,new String[]{"task"});
			JSONObject result=new JSONObject();
			result.put("task", taskjson);
			result.put("comments", commentjson);
			return result.toString();
		}
		else{
			return "false";
		}
	}
	
	@RequestMapping(value="/ajax/subjectbytask",method = { RequestMethod.POST, RequestMethod.GET },produces = "text/html;charset=UTF-8")
	@ResponseBody
	public Object getTasktoSubject(Integer tid){
		if(tid==null){
			return false;
		}
		List<Subject> subjects=taskService.getTosByTaskId(tid);
		if(subjects!=null){
			String[] subparameters={"classes","tests","grades"};
			JSONArray tosjson=processorParameter(subjects,subparameters);
			return tosjson.toString();
		}
		else{
			return "false";
		}
	}
	//任务详情页面
	@RequestMapping(value="/task/{id}")
	public ModelAndView taskContent(@PathVariable("id") Integer id){
		ModelAndView mv=new ModelAndView();	
			//注入默认对象
			mv=injectDefaultModel(mv);
			Task task=taskService.getTaskById(id);
			if(task!=null){
				List<Comment> comments=taskService.getCommentsByTaskId(id);
				mv.addObject("comments", comments);
				mv.addObject("task", task);
			}
			List<Subject> subjects=taskService.getTosByTaskId(id);
			if(subjects!=null){
				mv.addObject("toss", subjects);
			}
			mv.setViewName("/dashboard/task/task");
		return mv;
	}
	
	
	//提交评论
	@RequestMapping(value="/task/docomment",method = { RequestMethod.POST, RequestMethod.GET },produces = "text/html;charset=UTF-8")
	@ResponseBody
	public Object postComment(String editorValue,String tid,Integer uid){
		
		HttpSession session=request.getSession();
		if(session.getAttribute("onlineuser")!=null || uid!=null){
			System.out.println(editorValue);
			Task task=new Task();
			task.setTid(Integer.valueOf(tid));
			Comment comment=new Comment();
			comment.setBody(editorValue);
			comment.setTask(task);
			comment.setTime(new Date());
			//两者取其一
			if(session.getAttribute("onlineuser")!=null){
				User user=(User) session.getAttribute("onlineuser");
				comment.setUser(user);
			}
			else if(uid!=null){
				User user = new User();
				user.setUid(uid);
				comment.setUser(user);
			}
			try {
				taskService.saveComment(comment);
				return "true";
			} catch (Exception e) {
				return e.getMessage();
			}
			
		}
		//如果没有登陆过的用户提交要出错
		else{
			return "false";
		}
	}
}
