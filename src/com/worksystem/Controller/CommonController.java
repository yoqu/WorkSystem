package com.worksystem.Controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("common")
public class CommonController {
	
	@RequestMapping("404")
	public ModelAndView getnotfoundpage(){
		ModelAndView mav=new ModelAndView();
		mav.setViewName("global/404");
		return mav;
	}
	
	@RequestMapping(value = "/nopower")
	public ModelAndView createtablesData(){
		ModelAndView mav=new ModelAndView();
		mav.setViewName("global/nopower");
		return mav;
	}
	
}
