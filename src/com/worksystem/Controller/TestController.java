package com.worksystem.Controller;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.worksystem.Common.Common;
import com.worksystem.Common.Constant;
import com.worksystem.Service.GradeService;
import com.worksystem.Service.LoginService;
import com.worksystem.Service.SubjectService;
import com.worksystem.Service.TestService;
import com.worksystem.entity.Grade;
import com.worksystem.entity.Student;
import com.worksystem.entity.Subject;
import com.worksystem.entity.Test;
import com.worksystem.entity.User;

@Controller
@RequestMapping("/test")
public class TestController extends Common {
	
	@Resource
	private TestService testService;
	
	@Resource
	private GradeService gradeService;
	@Resource
	private SubjectService subjectService;
	@Resource
	private LoginService userService;
	//test默认主页
	@RequestMapping()
	public ModelAndView defaultmethod(HttpServletRequest request, HttpServletResponse response)  {
		ModelAndView mav=new ModelAndView();
		return mav;
	}
	
	@RequestMapping(value="{id}")
	public ModelAndView getSubjectTests(@PathVariable("id") Integer id ){
		Subject subject=subjectService.getByProerties("id", id);
		ModelAndView mav=new ModelAndView();
		mav=injectDefaultModel(mav);
		User user=(User)request.getSession().getAttribute(Constant.SESSION_USER);
		Student student = userService.getStudentByUid(user.getUid());
		if(student ==null){
			System.out.println("老师不可以考试哟");
			mav.addObject("message","老师不可以考试哟");
			mav.setViewName("/global/message");
			return mav;
		}
		Long num = gradeService.countAll(new String[]{"studentid","subjectid"}, new Object[]{student.getId(),id});
		System.out.println(num);
		if(num>0){
			System.out.println("已经考试过了,不能重复考试");
			mav.addObject("message","已经考了,不能重复考试");
			mav.setViewName("/global/message");
			return mav;
		}
		if(subject!=null){
			mav.addObject("subject", subject);
			System.out.println("考试科目名称："+subject.getName());
			List<Test> tests=testService.queryByProerties("subject.id", id);
			System.out.println("试卷题数量："+tests.size());
			mav.addObject("tests", writJson(tests, new String[]{"tests","subject","test"}));
			mav.setViewName("/test/test");
		}
		return mav;
	}
	@RequestMapping(value="/mygrade")
	public ModelAndView getmygrade(){
		User user=(User)request.getSession().getAttribute(Constant.SESSION_USER);
		ModelAndView mav =  new ModelAndView();
		mav=injectDefaultModel(mav);
		mav.setViewName("/test/mygrade");
		mav.addObject(user);
		List<Grade> grades = gradeService.queryByProerties("student.uid", user.getUid());
		mav.addObject("grades",grades);
		System.out.println(grades);
		return mav;
	}
		
		
	@RequestMapping(value="/ajax/gettests",method = { RequestMethod.POST, RequestMethod.GET },produces = "text/html;charset=UTF-8")
	@ResponseBody
	public Object getTestByJSON(Integer sid){
		Subject subject=subjectService.getByProerties("id", sid);
		if(subject!=null){
			List<Test> tests=testService.queryByProerties("subject.id", sid);
			String[] parameters={"tests","subject","test"};
			//过滤下参数
			net.sf.json.JSONArray testsjson=processorParameter(tests, parameters);
			String[] subparameters={"classes","tests","grades"};
			net.sf.json.JSONArray subjectjson=processorParameter(subject, subparameters);
			JSONObject jsonarrays=new JSONObject();
			jsonarrays.put("subject",subjectjson);
			jsonarrays.put("tests", testsjson);
			return jsonarrays.toString();
		}
		return "false";
	}
		//提交答案
		@RequestMapping(value="/finishtest",method = { RequestMethod.POST, RequestMethod.GET },produces = "text/html;charset=UTF-8")
		@ResponseBody
		public Object postfinishtest(String tests,String time,String subjectid){
			System.out.println("开始提交答案---了----------------------");
			System.out.println("tests:"+tests+"time:"+time+" subjectid"+subjectid);
				HttpSession session=request.getSession();
				User user=(User) session.getAttribute(Constant.SESSION_USER);
				Integer uid=user.getUid();
				System.out.println("用户uid"+uid	);
			return testService.postteststograde(tests, uid, Integer.valueOf(subjectid), time);
		}
}
