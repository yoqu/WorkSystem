package com.worksystem.util;

import java.io.InputStream;
import java.util.Iterator;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
public class Constant {
	public static final String SESSION_USER = "onlineuser";
	
	/**
	 * 通过实体得到数据库表的名称，在xml中进行配置
	 * @param entity
	 * @return
	 */
	public static String gettable(String entity){
		InputStream in=Constant.class.getClassLoader().getResourceAsStream("ORM.xml");
		try{  
		   SAXReader reader = new SAXReader();   
		   Document doc = reader.read(in); 
		   System.out.println(doc.getUniquePath());
		   Element root = doc.getRootElement();   
		   @SuppressWarnings("unchecked")
		   Iterator<Element> modulesIterator = root.elements("table").iterator();
		   while(modulesIterator.hasNext()){
			   Element table = modulesIterator.next();
			   if(table.attributeValue("entity").equals(entity)){
				   return table.attributeValue("name");
			   }
		   }
		  } catch (Exception e) {   
			  e.printStackTrace(); 
			  System.out.println(e.getMessage());
		  } 
		return null;
	}
	
}
