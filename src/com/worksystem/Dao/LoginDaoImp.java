package com.worksystem.Dao;


import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Component;

import com.worksystem.Dao.BaseDao;
import com.worksystem.entity.Classes;
import com.worksystem.entity.Department;
import com.worksystem.entity.Student;
import com.worksystem.entity.Teacher;
import com.worksystem.entity.User;
@Component
@SuppressWarnings("unchecked")
public class LoginDaoImp extends BaseDao<User> implements LoginDao {
	
	public LoginDaoImp() {
		super(User.class);
	}

	@Override
	
	public boolean dologin(User user) {
		System.out.println("开始执行登陆检测了");
		String LoginId=user.getLoginId();
		String pwd=user.getLoginPwd();
		System.out.println(LoginId);
		System.out.println(pwd);
		String hql="from User where loginId=:loginId and loginPwd=:loginPwd";
	    Query query= getSession().createQuery(hql).setParameter("loginId",LoginId).setParameter("loginPwd", pwd);
	    List<User> currentusers=query.list();
	    if(currentusers.size()>0){
	    	return true;
	    }
	    else{
	    	return false;
	    }
	}
	
	@Override
	public User getUserByLoginId(String LoginId){
		String hql="from User where LoginId=:loginId";
		Query query=getSession().createQuery(hql).setParameter("loginId", LoginId);
		List<User> users=query.list();
		if(users.size()>0){
			return users.get(0);
		}
		else{
			return null;
		}
	}
	@Override
	public boolean doregister(User user) {
		try {
			save(user);
			return true;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return false;
		}
	}
	@Override
	public User getUserByUid(Integer uid){
		return (User) getSession().get(User.class, uid);
	}
	
	
	/**
	 * 根据系部编号来 获取该系部的所有专业
	 * @param did 系部编号
	 * @return 专业集合
	 */
	@Override
	public List<Classes> getClassesbyDid(Integer did){
		String hql="from Classes where department.did=:did";
		Query query=getSession().createQuery(hql).setParameter("did", did);
		return query.list();
	}
	
	/**
	 * 根据专业Id来获取专业实体
	 * @param cid
	 * @return
	 */
	@Override
	public Classes getClassesById(Integer cid){
		return (Classes) getSession().get(Classes.class, cid);
	}
	
	/**
	 * 得到数据库系部列表
	 * @return
	 */
	@Override
	public List<Department> getAllDepartment(){
		String hql="from Department";
		return getSession().createQuery(hql).list();
	}
	@Override
	public List<Classes> getAllClasses(){
		String hql="from Classes";
		return getSession().createQuery(hql).list();
	}
	
	@Override
	public int doregisterstudent(User user,Student student){
		if(checkuserisregister(user)){
			//检测已注册返回100错误信息
			return 100;
		}
		getSession().save(user);
		System.out.println("注册的用户uid"+user.getUid());
		student.setUser(user);
		try {
			getSession().save(student);
			return 1;
		} catch (Exception e) {
			return 0;
		}
	}
	@Override
	public int doregisterteacher(User user ,Teacher teacher){
		if(checkuserisregister(user)){
			//检测已注册返回100错误信息
			return 100;
		}
		getSession().beginTransaction();
		save(user);
		teacher.setUid(user.getUid());
		try {
			//开始执行保存教师方法
			getSession().save(teacher);
			getSession().getTransaction().commit();
			return 1;
		} catch (Exception e) {
			getSession().getTransaction().rollback();
			return 0;			
		}
	}
	/**
	 * 检查用户是否已注册,已注册返回true，未注册返回false
	 * @param user
	 * @return
	 */
	public boolean checkuserisregister(User user){
		String hql="from User where loginId=:loginId";
		//检查用户是否已存在
		int resultcoloum=getSession().createQuery(hql).setParameter("loginId", user.getLoginId()).list().size();
		if(resultcoloum!=0){
			return true;
		}
		else{
			return false;
		}
	}
	@Override
	public List<Student> getStudentsByClasses(Integer cid){
		String hql="from Student where classes.cid=:cid";
		Query query=getSession().createQuery(hql).setParameter("cid", cid);//.setFirstResult(start).setMaxResults(length);
		return query.list();
	}
	public List<User> getStudentUsersByClasses(Integer cid, Integer start, Integer length) {
		String hql="select u from User as u,Student s where s.classes.cid=:cid and u.uid=s.user.uid";
		Query query=getSession().createQuery(hql).setParameter("cid", cid);
		return query.list();
	}
	@Override
	public Student getStudentByUid(Integer uid){
		String hql="from Student where uid=:uid";
		Student student=(Student) getSession().createQuery(hql).setParameter("uid", uid).uniqueResult();
		return student;
	}
	
	@Override
	public Teacher getTeacherByUid(Integer uid){
		String hql="from Teacher where uid=:uid";
		Teacher teacher=(Teacher)getSession().createQuery(hql).setParameter("uid", uid).uniqueResult();
		return teacher;
	}
	@Override
	public List<User> getPokermanUserByTask(Integer tid){
		String hql="select u from User as u,Pokermans as p where p.task.tid=:tid and p.user.uid=u.uid";
		Query query=getSession().createQuery(hql).setParameter("tid", tid);
		return query.list();
	}
	

	public boolean updateprofile(User user){
		try {
			getSession().update(user);
			getSession().flush();
			return true;
		} catch (Exception e) {
			System.out.println("更新错误+"+e.getMessage());
			return false;
		}
	}
}
