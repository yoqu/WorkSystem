package com.worksystem.Dao;

import org.springframework.stereotype.Component;

import com.worksystem.entity.Grade;

@Component
public class GradeDaoImp extends BaseDao<Grade> implements GradeDao{

	public GradeDaoImp() {
		super(Grade.class);
	}

}
