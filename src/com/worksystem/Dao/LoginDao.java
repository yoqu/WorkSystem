package com.worksystem.Dao;

import java.util.List;

import com.worksystem.entity.Classes;
import com.worksystem.entity.Department;
import com.worksystem.entity.Student;
import com.worksystem.entity.Teacher;
import com.worksystem.entity.User;

public interface LoginDao extends Dao<User>{
		public boolean dologin(User user);
		public User getUserByLoginId(String LoginId);
		
		/**
		 * 注册用户
		 * @param user 用户的Id
		 * @return 返回注册是否成功
		 */
		public boolean doregister(User user);
		
		/**
		 * 从uid获取用户对象
		 * @param uid
		 * @return
		 */
		public User getUserByUid(Integer uid);
		
		/**
		 * 根据系部编号来 获取该系部的所有专业
		 * @param did 系部编号
		 * @return 专业集合
		 */
		public List<Classes> getClassesbyDid(Integer did);
		
		/**
		 * 根据专业Id来获取专业实体
		 * @param cid
		 * @return
		 */
		public Classes getClassesById(Integer cid);
		
		/**
		 * 得到数据库系部列表
		 * @return
		 */
		public List<Department> getAllDepartment();
		
		public List<Classes> getAllClasses();
		
		public int doregisterstudent(User user,Student student);
		public int doregisterteacher(User user ,Teacher teacher);
		
		public boolean updateprofile(User user);
		public List<Student> getStudentsByClasses(Integer cid);
		public List<User> getStudentUsersByClasses(Integer cid, Integer start, Integer length);
		
		/**
		 * 根据用户实体获取学生对象
		 * @param uid
		 * @return
		 */
		public Student getStudentByUid(Integer uid);
		
		public Teacher getTeacherByUid(Integer uid);
		
		public List<User> getPokermanUserByTask(Integer tid);
}

