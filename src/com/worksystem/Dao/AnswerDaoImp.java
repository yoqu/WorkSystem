package com.worksystem.Dao;

import org.springframework.stereotype.Component;

import com.worksystem.entity.Answer;

@Component
public class AnswerDaoImp extends BaseDao<Answer> implements AnswerDao{
	public AnswerDaoImp() {
		super(Answer.class);
	}

}
