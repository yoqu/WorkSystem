package com.worksystem.Dao;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.worksystem.entity.PageBean;

public interface Dao<E> {
	/**
	 * 根据主键删除内容
	 * @param id
	 * @return
	 */
	public boolean deleteByPK(Serializable... id);

	public boolean save(E entity);
	
	public boolean delete(E entity);
	/**
	 * 根据列属性的值来进行删除
	 * @param propName 属性名称
	 * @param propValue 属性值
	 */
	public void deleteByProperties(String propName, Object propValue);

	/**
	 * 根据列属性的值来进行删除
	 * 
	 * @param propName 属性名称集合
	 * @param propValue 属性值集合
	 */
	public void deleteByProperties(String[] propName, Object[] propValue);

	/**
	 * Update the persistent instance with the identifier of the given detached instance.
	 * 
	 * @param entity
	 */
	public void update(E entity);

	/**
	 * 根据条件进行更新
	 * 
	 * @param conditionName where clause condiction property name
	 * @param conditionValue where clause condiction property value
	 * @param propertyName update clause property name array
	 * @param propertyValue update clase property value array
	 */
	public void updateByProperties(String[] conditionName, Object[] conditionValue, String[] propertyName, Object[] propertyValue);

	public void updateByProperties(String[] conditionName, Object[] conditionValue, String propertyName, Object propertyValue);

	public void updateByProperties(String conditionName, Object conditionValue, String[] propertyName, Object[] propertyValue);

	public void updateByProperties(String conditionName, Object conditionValue, String propertyName, Object propertyValue);

	/**
	 * cautiously use this method, through delete then insert to update an entity when need to update primary key value (unsupported) use this method
	 * 
	 * @param entity updated entity
	 * @param oldId already existed primary key
	 */
	public void update(E entity, Serializable oldId);

	/**
	 * 根据属性获取一个实体对象
	 * 
	 * @param propName
	 * @param propValue
	 * @return
	 */
	public E getByProerties(String[] propName, Object[] propValue);

	
	public E getByProerties(String[] propName, Object[] propValue, Map<String, String> sortedCondition);

	/**
	 * get an entity by property
	 * 
	 * @param propName
	 * @param propValue
	 * @return
	 */
	public E getByProerties(String propName, Object propValue);

	public E getByProerties(String propName, Object propValue, Map<String, String> sortedCondition);

	/**
	 * query by property
	 * 
	 * @param propName
	 * @param propValue
	 * @return
	 */
	public List<E> queryByProerties(String[] propName, Object[] propValue, Map<String, String> sortedCondition, Integer top);

	public List<E> queryByProerties(String[] propName, Object[] propValue, Map<String, String> sortedCondition);

	public List<E> queryByProerties(String[] propName, Object[] propValue, Integer top);

	public List<E> queryByProerties(String[] propName, Object[] propValue);

	public List<E> queryByProerties(String propName, Object propValue, Map<String, String> sortedCondition, Integer top);

	public List<E> queryByProerties(String propName, Object propValue, Map<String, String> sortedCondition);

	public List<E> queryByProerties(String propName, Object propValue, Integer top);

	public List<E> queryByProerties(String propName, Object propValue);
	/**
	 * count all
	 * 
	 * @return
	 */
	public Long countAll();

	public Long countAll(String propName, Object propValue);
	
	public Long countAll(String[] propName, Object[] propValue);
	/**
	 * Query all
	 * 
	 * @return
	 */
	public List<E> doQueryAll();

	public List<E> doQueryAll(Map<String, String> sortedCondition, Integer top);

	public List<E> doQueryAll(Integer top);

	public PageBean doPaginationQuery(@SuppressWarnings("rawtypes") List entitys,int page);

	public PageBean initPageBean(Integer pageId,Long rowscount);
}
