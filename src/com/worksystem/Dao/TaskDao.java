package com.worksystem.Dao;

import java.util.List;

import com.worksystem.entity.Comment;
import com.worksystem.entity.PageBean;
import com.worksystem.entity.Subject;
import com.worksystem.entity.Task;
import com.worksystem.entity.User;

public interface TaskDao extends Dao<Task> {
	/**
	 * 得到老师的近期任务列表
	 * @param user 用户
	 * @param start 开始位置
	 * @param length 长度
	 * @return 返回一个任务数组
	 */
	public PageBean getTeacherTasklist(User user,Integer pageid);
	
	/**
	 * 得到学生的近期任务
	 * @param user
	 * @param start
	 * @param length
	 * @return
	 */
	public PageBean getStudentTasklist(User user,Integer pageid);
	/**
	 * 获取某个任务的所有学生
	 * @param tid 任务id
	 * @param limit 限制所取人数，-1表示全部取
	 * @return 返回学生列表
	 */
	public User getUserByTaskId(int tid);
	/**
	 * 由任务Id获取任务
	 * @param id
	 * @return
	 */
	public Task getTaskById(Integer id);
	
	public List<Comment> getCommentsByTaskId(Integer id);
	
	public void saveComment(Comment comment);
	
	/**
	 * 新增任务
	 * @param task
	 * @return
	 */
	public Integer inserttask(Task task,Object[] pokermanids,Object[] subjectids);
	
	public Integer deletetask(Integer tid);
	
	public Integer updatetask(Task task,Object[] pokermanids);
	
	public List<Subject> getTosByTaskId(Integer tid);
	
	public List<Subject> getSubjectsByClassId(Integer classId);
}
