package com.worksystem.Dao;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.jdbc.core.JdbcTemplate;

import com.worksystem.entity.PageBean;
import com.worksystem.util.Constant;
import com.worksystem.util.SqlHandle;

public class BaseDao<E> implements Dao<E>{
	
	public static final String MAPPER_PATH="org.yoqu.entity.mapper.BaseMapper";
	protected Class<E> entityClass;
	private String table;
	public BaseDao(Class<E> entityClass){
		this.entityClass = entityClass;
		String entity=entityClass.getName();
		table=Constant.gettable(entity);
	}
	private Map<String, Object> parameters = new HashMap<String, Object>();
	
	public Map<String, Object> getParameters(){
		parameters.clear();
		parameters.put("table", entityClass.getName());
		return parameters;
	}
	@Resource
	private SessionFactory sessionFactory;
	public Session getSession(){
		return sessionFactory.getCurrentSession();
	}
	@Resource
	private JdbcTemplate jdbcTemplate;
	@Override
	public Long countAll() {
		SqlHandle sql = new SqlHandle(table);
		sql.FIELD("count(*)");
		return Long.parseLong(getSession().createSQLQuery(sql.toString()).uniqueResult().toString());
	}
	public Long countAll(String propName, Object propValue){
		SqlHandle sql = new SqlHandle(table);
		sql.FIELD("count(*)").
		CONDITION(propName, "=", propValue);
		return Long.parseLong(getSession().createSQLQuery(sql.toString()).uniqueResult().toString());
	}
	public Long countAll(String[] propName, Object[] propValue){
		SqlHandle sql = new SqlHandle(table);
		sql.FIELD("count(*)");
		for (int i = 0; i < propValue.length; i++) {
			sql.CONDITION(propName[i], "=", propValue[i]);
		}
		return Long.parseLong(getSession().createSQLQuery(sql.toString()).uniqueResult().toString());
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<E> doQueryAll() {
		String sql ="from "+table;
		System.out.println("执行doQueryAll()方法 "+sql);
		return getSession().createQuery(sql).list();
	}
	

	public boolean delete(E entity){
		try {
			System.out.println("开始删除试题操作,执行delete方法");
			getSession().delete(entity);
			return true;
		} catch (Exception e) {
			System.out.println("删除实体错误,"+e.getMessage());
			return false;
		}
	}
	@Override
	public boolean deleteByPK(Serializable... id) {
		String sql=" delete from "+table+" where id in(";
		if(id.length>0){
			for(int i=0;i<id.length-1;i++){
				sql+=id[i]+",";	
			}
			sql+=id[id.length-1]+")";
		}
		System.out.println("删除方法deleteByPK执行.sql:"+sql);
		int num =getSession().createSQLQuery(sql).executeUpdate();
		System.out.println("删除受影响行数为:"+num);
		if(num>0)
			return true;
		else
			return false;
	}

	@Override
	public boolean save(E entity) {
		try {
			getSession().save(entity);
			return true;
		} catch (Exception e) {
			System.out.println("保存错误");
			return false;
		}
	
	}

	@Override
	public void deleteByProperties(String propName, Object propValue) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteByProperties(String[] propName, Object[] propValue) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(E entity) {
		getSession().update(entity);
		getSession().flush();
	}

	@Override
	public void updateByProperties(String[] conditionName,
			Object[] conditionValue, String[] propertyName,
			Object[] propertyValue) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateByProperties(String[] conditionName,
			Object[] conditionValue, String propertyName, Object propertyValue) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateByProperties(String conditionName, Object conditionValue,
			String[] propertyName, Object[] propertyValue) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateByProperties(String conditionName, Object conditionValue,
			String propertyName, Object propertyValue) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(E entity, Serializable oldId) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public E getByProerties(String[] propName, Object[] propValue) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public E getByProerties(String[] propName, Object[] propValue,
			Map<String, String> sortedCondition) {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public E getByProerties(String propName, Object propValue) {
		String sql="from "+table+" where "+propName+"=:propValue";
		System.out.println("执行getByProerties(String propName, Object propValue)方法"+sql);
		return (E) getSession().createQuery(sql).setParameter("propValue", propValue).uniqueResult();
	}

	@Override
	public E getByProerties(String propName, Object propValue,
			Map<String, String> sortedCondition) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<E> queryByProerties(String[] propName, Object[] propValue,
			Map<String, String> sortedCondition, Integer top) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<E> queryByProerties(String[] propName, Object[] propValue,
			Map<String, String> sortedCondition) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<E> queryByProerties(String[] propName, Object[] propValue,
			Integer top) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<E> queryByProerties(String[] propName, Object[] propValue) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<E> queryByProerties(String propName, Object propValue,
			Map<String, String> sortedCondition, Integer top) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<E> queryByProerties(String propName, Object propValue,
			Map<String, String> sortedCondition) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<E> queryByProerties(String propName, Object propValue,
			Integer top) {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<E> queryByProerties(String propName, Object propValue) {
		String sql = "from "+table+" where "+propName+"=:propValue";
		System.out.println(sql);
		return getSession().createQuery(sql).setParameter("propValue", propValue).list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<E> doQueryAll(Map<String, String> sortedCondition, Integer top) {
		return getSession().createQuery("from "+table).list();
	}

	@Override
	public List<E> doQueryAll(Integer top) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PageBean doPaginationQuery(@SuppressWarnings("rawtypes") List entitys, int page) {
		PageBean pagebean = initPageBean(page, Long.parseLong(Integer.toString(entitys.size())));
		pagebean.setList(entitys);
		return pagebean;
	}

	@Override
	public PageBean initPageBean(Integer pageId,Long rowscount){		
		PageBean pagebean = new PageBean();
		pagebean.setCurrentPage(pageId);
		 Integer PAGESIZE=PageBean.PAGESIZE;//每页条数设置
		pagebean.setPageSize(PAGESIZE);
		if(rowscount!=null){
			 Integer start =pageId*PageBean.PAGESIZE;
			 pagebean.setStart(start);
			//如果条数大于则设置共有多少页，否则设置只有一页
			if(rowscount>PAGESIZE){
				Integer pagetotal=0;
				if(rowscount%PAGESIZE>0){
					pagetotal=(int) (rowscount / PAGESIZE)+1;
				}
				else{
					pagetotal=(int) (rowscount / PAGESIZE);
				}
				pagebean.setTotalPage(pagetotal);
				if(pageId==1){
					pagebean.setFirstPage(true);
					pagebean.setLastPage(false);
					if(pageId.equals(pagetotal))
					pagebean.setHasNextPage(false);
					else
					pagebean.setHasNextPage(true);
					pagebean.setHasPreviousPage(false);
				}
				else if(pageId.equals(pagetotal)){
					pagebean.setLastPage(true);
					pagebean.setFirstPage(false);
					pagebean.setHasNextPage(false);
					pagebean.setHasPreviousPage(true);
				}
				else{
					pagebean.setHasNextPage(true);
					pagebean.setHasPreviousPage(true);
				}
				
			}
			else{
				pagebean.setTotalPage(1);
				pagebean.setHasNextPage(false);
				pagebean.setHasPreviousPage(false);
			}
		}
		
		return pagebean;
	}
}
