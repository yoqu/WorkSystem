package com.worksystem.Dao;

import org.hibernate.Query;
import org.springframework.stereotype.Component;

import com.worksystem.Common.BaseDao;

@SuppressWarnings("rawtypes")
@Component
public class CommonDaoImp extends BaseDao implements CommonDao{

	
	@Override
	public boolean createdata() {
		String hql="INSERT INTO department VALUES ('1', '信息工程系');"
				+ "INSERT INTO department VALUES ('2', '人文艺术系');"
				+ "INSERT INTO classes VALUES ('1', '1', '软件技术');"
				+ "INSERT INTO classes VALUES ('2', '1', '图形图像');"
				+ "INSERT INTO user VALUES ('1', '川帅', 'yoqu', '123456', 'student', '2015-10-12 11:11:46');";
		Query query=getSession().createSQLQuery(hql);
		if(query.executeUpdate()!=0){
			return true;
		}
		else{
			return false;
		}
	}

}
