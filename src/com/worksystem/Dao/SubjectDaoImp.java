package com.worksystem.Dao;

import java.io.Serializable;

import org.springframework.stereotype.Component;

import com.worksystem.entity.Subject;
import com.worksystem.util.SqlHandle;

@Component("subjectDao")
public class SubjectDaoImp extends BaseDao<Subject> implements SubjectDao {

	public SubjectDaoImp() {
		super(Subject.class);
		// TODO Auto-generated constructor stub
	}
	/**
	 * 首先删除答案
	 * 其次删除每一道题
	 * 再删除试卷
	 */
	@Override
	public boolean deleteByPK(Serializable... id) {
		//先判断任务是否和试题绑定了，如果绑定需要先删除任务。
		SqlHandle istos = new SqlHandle("tos");
		istos.FIELD("count(*)");
		istos.CONDITION("sid", "in", id);
		//如果查询出来结果
		if(Integer.parseInt(getSession().createSQLQuery(istos.toString()).uniqueResult().toString())>0){
			System.out.println("已经有任务绑定了试题了，不可以删除试卷");
			return false;
		}
		//1.删除答案
		SqlHandle deleteanswer=new SqlHandle(SqlHandle.OPERATES[3],"answer");
		deleteanswer.CONDITION("sid", "in", id);
		//2.删除每一道题
		SqlHandle deletetest = new SqlHandle(SqlHandle.OPERATES[3],"test");
		deletetest.CONDITION("sid", "in", id);
		//3.删除试卷
		SqlHandle deleteSubject =  new SqlHandle(SqlHandle.OPERATES[3],"subject");
		deleteSubject.CONDITION("id", "in", id);
		try {
			getSession().beginTransaction();
			if(getSession().createSQLQuery(deleteanswer.toString()).executeUpdate()>0){
				System.out.println("先删除题库题目的答案，"+deleteanswer.toString());
			}
			if(getSession().createSQLQuery(deletetest.toString()).executeUpdate()>0){
				System.out.println("删除题库中的题目了"+deletetest.toString());
			}
			int num=getSession().createSQLQuery(deleteSubject.toString()).executeUpdate();
			getSession().getTransaction().commit();
			if(num>0){
				System.out.println("删除该套试卷了 "+deleteSubject.toString());
				return true;
			}
			else
				return false;
		} catch (Exception e) {
			getSession().getTransaction().rollback();
			e.printStackTrace();
			System.out.println("删除试卷出问题了，问题为"+e.getMessage());
			return false;
		}
	}

}
