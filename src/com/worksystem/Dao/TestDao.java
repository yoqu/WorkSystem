package com.worksystem.Dao;

import com.worksystem.entity.Test;

public interface TestDao extends Dao<Test> {
	
	/**
	 *	提交考试数据到Dao层进行计算，并将成绩录入数据库
	 * @param testjson
	 * @return
	 */
	public Object postteststograde(String testjson,Integer uid,Integer subjectid,String time);
	
	
}
