package com.worksystem.entity;

import java.util.HashSet;
import java.util.Set;
@SuppressWarnings("rawtypes")
public class Test implements java.io.Serializable {

	private static final long serialVersionUID = 5519238668519524451L;
	private Integer id;
	private Subject subject;
	private String content;
	private Integer grade;
	private Integer answerid;
	private Set answers = new HashSet(0);


	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Subject getSubject() {
		return this.subject;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Integer getGrade() {
		return this.grade;
	}

	public void setGrade(Integer grade) {
		this.grade = grade;
	}

	public Integer getAnswerid() {
		return this.answerid;
	}

	public void setAnswerid(Integer answerid) {
		this.answerid = answerid;
	}

	public Set getAnswers() {
		return this.answers;
	}

	public void setAnswers(Set answers) {
		this.answers = answers;
	}

}