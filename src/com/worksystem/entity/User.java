package com.worksystem.entity;

// Generated 2015-9-22 9:28:35 by Hibernate Tools 3.4.0.CR1



import java.util.Date;

/**
 * User generated by hbm2java
 */
public class User  {

	private Integer uid;
	private String name;
	private String loginId;
	private String loginPwd;
	private Roles role;
	private Date created;

	public User() {
	}

	public User(String name, String loginId, String loginPwd, Roles role,
			Date created) {
		this.name = name;
		this.loginId = loginId;
		this.loginPwd = loginPwd;
		this.role = role;
		this.created = created;
	}

	public Integer getUid() {
		return this.uid;
	}

	public void setUid(Integer uid) {
		this.uid = uid;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLoginId() {
		return this.loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getLoginPwd() {
		return this.loginPwd;
	}

	public void setLoginPwd(String loginPwd) {
		this.loginPwd = loginPwd;
	}

	public Roles getRole() {
		return this.role;
	}

	public void setRole(Roles role) {
		this.role = role;
	}

	public Date getCreated() {
		return this.created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	@Override
	public String toString(){
		return "uid"+uid+" Name:"+name+" LoginId :"+loginId+" LoginPwd:"+loginPwd+" Role:"+role;
	}
}
