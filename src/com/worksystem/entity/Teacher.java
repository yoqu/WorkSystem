package com.worksystem.entity;

public class Teacher {
	private Integer id;
	private Classes classes;
	private Integer age;
	private int uid;
	private Department department;
	private String teatitle;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Classes getClasses() {
		return classes;
	}
	public void setClasses(Classes classes) {
		this.classes = classes;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	
	public int getUid() {
		return uid;
	}
	public void setUid(int uid) {
		this.uid = uid;
	}
	public Department getDepartment() {
		return department;
	}
	public void setDepartment(Department department) {
		this.department = department;
	}
	public String getTeatitle() {
		return teatitle;
	}
	public void setTeatitle(String teatitle) {
		this.teatitle = teatitle;
	}
}
