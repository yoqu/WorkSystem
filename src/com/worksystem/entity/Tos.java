package com.worksystem.entity;
/**
 * Task to Subject的中间类，用来关联 任务和考试科目的
 * @author 小川
 */
public class Tos  implements java.io.Serializable{
	private static final long serialVersionUID = 1L;
	private Integer id;
	private Task task;
	private Subject subject;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Task getTask() {
		return task;
	}
	public void setTask(Task task) {
		this.task = task;
	}
	public Subject getSubject() {
		return subject;
	}
	public void setSubject(Subject subject) {
		this.subject = subject;
	}
	
}
