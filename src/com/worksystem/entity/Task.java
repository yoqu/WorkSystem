package com.worksystem.entity;

import java.util.Date;

public class Task {
	private Integer tid;
	private User user;
	private String title;
	private String body;
	private Date time;
	private Date limittime;
	private Integer pokerman;
	private Integer comment;
	private Integer status;
	private Integer classesid;

	

	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Integer getTid() {
		return tid;
	}
	public void setTid(Integer tid) {
		this.tid = tid;
	}

	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public Date getTime() {
		return time;
	}
	public void setTime(Date time) {
		this.time = time;
	}
	public Date getLimittime() {
		return limittime;
	}
	public void setLimittime(Date limittime) {
		this.limittime = limittime;
	}
	public Integer getPokerman() {
		return pokerman;
	}
	public void setPokerman(Integer pokerman) {
		this.pokerman = pokerman;
	}
	public Integer getComment() {
		return comment;
	}
	public void setComment(Integer comment) {
		this.comment = comment;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Integer getClassesid() {
		return classesid;
	}
	public void setClassesid(Integer classesid) {
		this.classesid = classesid;
	}
	@Override
	public String toString(){
		return this.title+" body:"+this.body+" time:"+this.time+" pokerman:"+comment+" status: "+status;
	}
}
