package com.worksystem.Common;

public interface Dao<T> {
	
	/**
	 * 删除一个对象
	 * @param entity
	 */
	public void delete(T entity);
	/**
	 * 保存一个实体
	 * @param entity
	 */
	public void save(T entity);
	
	/**
	 * 更新一个实体对象
	 * @param entity
	 */
	public void update(T entity);
	
}
