package com.worksystem.Common;

import java.util.HashMap;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.worksystem.entity.PageBean;
import com.worksystem.util.Constant;

@Component
public class BaseDao<T> extends Constant implements Dao<T> {
	@Autowired
	private SessionFactory sessionFactory;
	
	public HashMap<String, Object> parameters = new HashMap<String, Object>();
	
	public Session getSession() {
		return sessionFactory.getCurrentSession();
	}

	public List<T> getObjectsByhqlParameter(Query query,
		HashMap<String, Object> parameters) {
		// 赋参数进query
		for (java.util.Map.Entry<String, Object> parameter : parameters
				.entrySet()) {
			query.setParameter(parameter.getKey(), parameter.getValue());
		}
		@SuppressWarnings("unchecked")
		List<T> objects = query.list();
		return objects;
	}
	
	/**
	 * 根据hql语句来获取对象实体集合，
	 * @param hql hql语句 
	 * @param parameters  hql需要传递的参数可以为null
	 * @param start 开始位置 可以为null
	 * @param length 长度 可以为null
	 * @return
	 */
	public Query getEntityByHql(String hql,HashMap<String, Object> parameters,Integer start,Integer length){
		Query query=getSession().createQuery(hql);
		if(parameters!=null){
			for (java.util.Map.Entry<String, Object> parameter : parameters
					.entrySet()) {
				query.setParameter(parameter.getKey(), parameter.getValue());
			}
		}
		if(start!=null && length!=null){
			query=query.setFirstResult(start).setMaxResults(length);
		}
		return query;
	}
	
	/**
	 * 查询有多少条记录
	 * @param hql 使用count函数的hql语句
	 * @param parameters 参数可以为null
	 * @return
	 */
	public Long getEntitiessize(String hql,HashMap<String, Object> parameters){
		Query query=getSession().createQuery(hql);
		if(parameters!=null){
			for (java.util.Map.Entry<String, Object> parameter : parameters
					.entrySet()) {
				query.setParameter(parameter.getKey(), parameter.getValue());
			}
		}
		Long count= (Long) query.uniqueResult();
		return count;
	}
	
	public PageBean initPageBean(Integer pageId,Long rowscount){		
		PageBean pagebean = new PageBean();
		pagebean.setCurrentPage(pageId);
		 Integer PAGESIZE=PageBean.PAGESIZE;//每页条数设置
		pagebean.setPageSize(PAGESIZE);
		if(rowscount!=null){
			 Integer start = (pageId-1)*PageBean.PAGESIZE;
			 pagebean.setStart(start);
			//如果条数大于则设置共有多少页，否则设置只有一页
			if(rowscount>PAGESIZE){
				Integer pagetotal = (int) (rowscount / PAGESIZE);
				pagebean.setTotalPage(pagetotal);
				if(pageId==pagetotal){
					pagebean.setLastPage(true);
					pagebean.setHasNextPage(false);
					pagebean.setHasPreviousPage(true);
				}
				if(pageId==1){
					pagebean.setFirstPage(true);
					pagebean.setHasNextPage(true);
					pagebean.setHasPreviousPage(false);
				}
			}
			else{
				pagebean.setTotalPage(1);
				pagebean.setHasNextPage(false);
				pagebean.setHasPreviousPage(false);
			}
		}
		
		return pagebean;
	}
	
	public PageBean insertQueryByhql(Integer pageId,String hql,String counthql,HashMap<String, Object> parameters){
		Long count=getEntitiessize(counthql, parameters);
		if(count==0){
			return null;
		}
		else{
			PageBean page=initPageBean(pageId,count);
			Query query=getEntityByHql(hql,parameters, page.getStart(), PageBean.PAGESIZE);
			page.setList(query.list());;
			return page;
		}
	}
	@Override
	public void delete(Object entity) {
		getSession().delete(entity);
	}

	@Override
	public void save(Object entity) {
		getSession().save(entity);
	}

	@Override
	public void update(Object entity) {
		getSession().update(entity);
	}

}
