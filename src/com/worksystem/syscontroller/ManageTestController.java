package com.worksystem.syscontroller;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.worksystem.Common.Common;
import com.worksystem.Common.Constant;
import com.worksystem.Service.AnswerService;
import com.worksystem.Service.LoginService;
import com.worksystem.Service.SubjectService;
import com.worksystem.Service.TestService;
import com.worksystem.entity.Answer;
import com.worksystem.entity.Classes;
import com.worksystem.entity.PageBean;
import com.worksystem.entity.Subject;
import com.worksystem.entity.Teacher;
import com.worksystem.entity.Test;
import com.worksystem.entity.User;

@Controller
@RequestMapping("manage/test")
public class ManageTestController extends Common{
	
	@Resource
	private TestService testService;
	@Resource
	private LoginService userService;
	@Resource
	private SubjectService subjectService;
	@Resource
	private AnswerService answerService;
	/**
	 * 试卷列表
	 * @return
	 */
	@RequestMapping(value="/testlist")
	public ModelAndView testslist(){
		//System.out.println("asdasd");
		@SuppressWarnings("unused")
		HttpSession session=request.getSession();
		ModelAndView mav=new ModelAndView();
		mav=injectDefaultModel(mav);
		Integer pageId=1;
		if(request.getParameter("page")!=null){
		  pageId=Integer.valueOf(request.getParameter("page"));
		}
		List<Subject> subjects=subjectService.doQueryAll();
		PageBean pagebean =subjectService.doPaginationQuery(subjects, pageId);
		mav.addObject("pagebean",pagebean);
		mav.setViewName("dashboard/test/testmanage");
		return mav;
	}

	@RequestMapping(value="/subjectadd")
	public ModelAndView subjectadd(){
		HttpSession session=request.getSession();
		ModelAndView mav=new ModelAndView();
		User user=(User) session.getAttribute(Constant.SESSION_USER);
		if(user.getRole().getRid()==1){
			Teacher teacher=userService.getTeacherByUid(user.getUid());
			Classes classes = teacher.getClasses();
			mav.addObject("classes",classes);
		}
		mav=injectDefaultModel(mav);
		mav.setViewName("/dashboard/test/subjectadd");
		return mav;
	}
	
	@RequestMapping(value="/testadd")
	public ModelAndView testadd(){
		ModelAndView mav=new ModelAndView();
		mav=injectDefaultModel(mav);
		mav.setViewName("/dashboard/test/testadd");
		return mav;
	}
	@RequestMapping(value="/edit/{id}")
	public ModelAndView subjectedit(@PathVariable("id") Integer sid){
		ModelAndView mav = new ModelAndView();
		mav=injectDefaultModel(mav);
		Subject subject  =  subjectService.getByProerties("id", sid);
		List<Test> tests = testService.queryByProerties("subject.id", subject.getId());
		if(tests.size()>0){
			mav.addObject("tests",tests);
		}
		mav.addObject("subject",subject);
		mav.setViewName("dashboard/test/subjectedit");
		return mav;
	}
	
	@RequestMapping(value="dodeletetest", method = { RequestMethod.POST, RequestMethod.GET },produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String dodeletetest(Integer tid){
		if(tid!=null){
			if(testService.deleteByPK(tid))
				return "1";
			else
				return "0";
		}else{
			return "0";
		}
	}
	@RequestMapping(value="doaddsubject", method = { RequestMethod.POST, RequestMethod.GET },produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String subjectdoadd(Subject subject,Integer classesid){
		Classes classes = new Classes();
		classes.setCid(classesid);
		subject.setClasses(classes);
		System.out.println("开始保存科目");
		if(subjectService.save(subject)){
			return  writJson(subject,new String[]{"classes","tests","grades"});
		}
		else
		return "false";
	}
	
	@RequestMapping(value="doeditsubject", method = { RequestMethod.POST, RequestMethod.GET },produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String subjectdoedit(Integer sid,String name,Double credit,Integer classesid){
		Subject subject =subjectService.getByProerties("id", sid);
		subject.setName(name);
		subject.setCredit(credit);
		Classes classes = userService.getClassesById(classesid);
		subject.setClasses(classes);
		System.out.println("开始保存科目");
		try {
			subjectService.update(subject);
			return "1";
		} catch (Exception e) {
			return "-1";
		}
	}
	
	
	@RequestMapping(value="doaddtest", method = { RequestMethod.POST, RequestMethod.GET },produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String testdoadd(String content,Integer grade,Integer sid,String contenta,String contentb,String contentc,String contentd,Integer correctanswer){
		Subject subject = subjectService.getByProerties("id", sid);
		Test test = new Test();
		test.setContent(content);
		test.setGrade(grade);
		test.setSubject(subject);
		testService.save(test);
		Answer aa=new Answer(contenta,subject,test);
		Answer ab=new Answer(contentb,subject,test);
		Answer ac=new Answer(contentc,subject,test);
		Answer ad=new Answer(contentd,subject,test);
		answerService.save(aa);
		answerService.save(ab);
		answerService.save(ac);
		answerService.save(ad);
		ArrayList<Answer> answers = new ArrayList<Answer>();
		answers.add(aa);
		answers.add(ab);
		answers.add(ac);
		answers.add(ad);
		test.setAnswerid(answers.get(correctanswer).getId());
		try {
			testService.update(test);
			return "1";
		} catch (Exception e) {
			return "-1";
		}
		
	}
	
	@RequestMapping(value="doedittest", method = { RequestMethod.POST, RequestMethod.GET },produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String testdoedit(Integer tid,String content,String contenta,String contentb,String contentc,String contentd,Integer correctanswer){
		Test test = testService.getByProerties("id", tid);
		test.setAnswerid(correctanswer);
		test.setContent(content);
		@SuppressWarnings("unchecked")
		Set<Answer> answers=test.getAnswers();
		int i=0;
		for (Answer answer : answers) {
			if(i==0)
			answer.setContent(contenta);
			else if(i==1)
				answer.setContent(contentb);
			else if(i==2)
				answer.setContent(contentc);
			else if(i==3)
				answer.setContent(contentd);
			answerService.update(answer);
			if(i==correctanswer)
			test.setAnswerid(answer.getId());
			i++;
		}
		try {
			testService.update(test);
			return "1";
		} catch (Exception e) {
			return "-1";
		}
		
		
	}
	@RequestMapping(value = "/dosubjectdelete", method = { RequestMethod.POST, RequestMethod.GET },produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String dosubjectdelete(Integer sid){
		System.out.println("执行删除subject方法");
		if(subjectService.deleteByPK(sid)){
			return "1";
		}
		else{
			return "false";
		}
	}
	
	@RequestMapping(value = "/dotestdelete", method = { RequestMethod.POST, RequestMethod.GET },produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String dotestdelete(Integer tid){
		System.out.println("执行删除subject方法");
		if(testService.deleteByPK(tid)){
			return "1";
		}
		else{
			return "false";
		}
	}
	
	
}
