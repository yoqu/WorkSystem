package com.worksystem.syscontroller;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.worksystem.Common.Common;
import com.worksystem.Common.Constant;
import com.worksystem.Service.LoginService;
import com.worksystem.entity.PageBean;
import com.worksystem.entity.Student;
import com.worksystem.entity.Subject;
import com.worksystem.entity.Teacher;
import com.worksystem.entity.User;


@Controller
@RequestMapping("manage/student")
public class ManageStudentController extends Common {

	@Resource
	private LoginService userService;
	//进入默认主页
	@RequestMapping()
	public ModelAndView defaultmethod()  {
		ModelAndView mv = new ModelAndView();
		mv.setViewName("/dashboard/user/usermanage");
		mv = injectDefaultModel(mv);
		HttpSession session=request.getSession();
		User user =(User)session.getAttribute(Constant.SESSION_USER);
		Teacher teacher =userService.getTeacherByUid(user.getUid());
		if(teacher==null){
			mv.addObject("message","教师才可以管理学生");
			mv.setViewName("global/message");
			return mv;
		}
		int page=1;
		if(request.getParameter("page")!=null){
			page =Integer.valueOf(request.getParameter("page"));
		}
		List<Student> students =userService.getStudentsByClasses(teacher.getClasses().getCid());
		PageBean pagebean = userService.doPaginationQuery(students, page);
		mv.addObject("pagebean",pagebean);
		return mv;
	}
	
		@RequestMapping(value="/add")
		public ModelAndView testslist(){
			initDefaultModel();
			mav.setViewName("/dashboard/user/useradd");
			HttpSession session=request.getSession();
			User user =(User)session.getAttribute(Constant.SESSION_USER);
			Teacher teacher =userService.getTeacherByUid(user.getUid());
			mav.addObject("classes",teacher.getClasses());
			mav.addObject("department",teacher.getDepartment());
			return mav;
		}

}
