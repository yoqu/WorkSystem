package com.worksystem.syscontroller;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.worksystem.Common.Common;
import com.worksystem.Common.Constant;
import com.worksystem.Service.LoginService;
import com.worksystem.Service.TaskService;
import com.worksystem.entity.PageBean;
import com.worksystem.entity.Subject;
import com.worksystem.entity.Task;
import com.worksystem.entity.Teacher;
import com.worksystem.entity.User;

@Controller
@RequestMapping("manage/task")
public class ManageTaskController extends Common {
	@Resource
	private TaskService taskService;
	@Resource
	private LoginService userDao;
	//任务详情页面
	@RequestMapping(value="/tasklist")
	public ModelAndView tasklist(){
			ModelAndView mv=new ModelAndView();
			mv=injectDefaultModel(mv);
			HttpSession session=request.getSession();			
			User user=(User) session.getAttribute(Constant.SESSION_USER);
			Integer pageid=1;
			if(request.getParameter("page")!=null){
				pageid=Integer.parseInt(request.getParameter("page"));
			}
			PageBean pagebean = taskService.getUserTasklist(user, pageid);
			mv.addObject("pagebean", pagebean);
			mv.setViewName("dashboard/task/taskmanage");
			return mv;
	}
	
	@RequestMapping(value="/add")
	public ModelAndView taskadd(){
			ModelAndView mv=new ModelAndView();
			mv=injectDefaultModel(mv);
			HttpSession session=request.getSession();
			//检查是否登录，未登录返回登录页面
			if(checkpermission(mv)!=null){
				return mv;
			}
			User user=(User) session.getAttribute(Constant.SESSION_USER);
			Teacher teacher=userDao.getTeacherByUid(user.getUid());
			List<User> users=userDao.getStudentUsersByClasses(teacher.getClasses().getCid(), null, null);
			mv.addObject("users", users);
			List<Subject> subjects=taskService.getSubjectsByClassId(teacher.getClasses().getCid());
			System.out.println(subjects.get(0).getName());
			mv.addObject("subjects",subjects);
			mv.setViewName("dashboard/task/taskadd");
			return mv;
	}
	
	@RequestMapping(value="/edit/{id}")
	public ModelAndView taskedit(@PathVariable("id") Integer tid){
			ModelAndView mv=new ModelAndView();
			mv=injectDefaultModel(mv);
			HttpSession session=request.getSession();			
			User user=(User) session.getAttribute(Constant.SESSION_USER);
			Teacher teacher=userDao.getTeacherByUid(user.getUid());
			List<User> classusers=userDao.getStudentUsersByClasses(teacher.getClasses().getCid(), null, null);
			System.out.println(classusers.size());
			List<User> pokerusers=userDao.getPokermanUserByTask(tid);
			for (User pokeruser : pokerusers) {
				classusers.remove(pokeruser);
			}
			System.out.println(classusers.size());
			Task task=taskService.getTaskById(tid);
			mv.addObject("notpokerusers", classusers);
			mv.addObject("pokerusers",pokerusers);
			mv.addObject("task", task);
			mv.setViewName("dashboard/task/taskedit");
			return mv;
	}
	
	
	
	@RequestMapping(value = "/dotaskdelete", method = { RequestMethod.POST, RequestMethod.GET },produces = "text/html;charset=UTF-8")
	@ResponseBody
	public Object dotaskdelete(Integer tid,Integer uid){
		HttpSession session=request.getSession();
		//检查是否登录，未登录返回登录页面
		if(session.getAttribute("onlineuser")!=null){
			User user=(User) session.getAttribute("onlineuser");
			return taskService.deletetask(tid, user.getUid()).toString();
		}
		else if(uid!=null){
			return taskService.deletetask(tid,uid).toString();
		}
		else{
			return "105";
		}
	}
	@RequestMapping(value = "/dotaskadd", method = { RequestMethod.POST, RequestMethod.GET },produces = "text/html;charset=UTF-8")
	@ResponseBody
	public Object dotaskadd(String title,String time,String body,String uids,Integer uid,String subids){
		JSONArray uidsjson=JSONArray.fromObject(uids);
		Object[] uidarray=uidsjson.toArray();
		Object[] subjectids=null;
		if(subids!=null){
			subjectids=JSONArray.fromObject(subids).toArray();
		}
		Integer result=0;
		if(uid!=null){
			 result=taskService.Inserttask(title, uid, time, body, uidarray,subjectids);	
		}else{			
			HttpSession session=request.getSession();
				User user=(User) session.getAttribute(Constant.SESSION_USER);
				result=taskService.Inserttask(title, user.getUid(), time, body, uidarray,subjectids);
			
		}
		return result.toString();
	}
	@RequestMapping(value = "/dotaskupdate", method = { RequestMethod.POST, RequestMethod.GET },produces = "text/html;charset=UTF-8")
	@ResponseBody
	public Object dotaskupdate(Integer tid,String title,String time,String body,String uids,Integer uid){
		JSONArray uidsjson=JSONArray.fromObject(uids);
		Object[] uidarray=uidsjson.toArray();
		Integer result=0;
		if(uid!=null){
			 result=taskService.updatetask(tid, title, uid, time, body, uidarray);	
		}else{
			HttpSession session=request.getSession(); 
				User user=(User) session.getAttribute(Constant.SESSION_USER);
				result=taskService.updatetask(tid, title, user.getUid(), time, body, uidarray);
			
		}
		return result.toString();
	}
	
	@RequestMapping(value="/ajax/getClassesusers",method = { RequestMethod.POST, RequestMethod.GET },produces = "text/html;charset=UTF-8")
	@ResponseBody
	public Object getClassesByJSON(Integer cid){
		if(cid!=null){
			List<User> users=userDao.getStudentUsersByClasses(cid, null, null);
			if(users.size()>0){
				JSONArray ja=processorParameter(users,null);
				return ja.toString();
			}
			else{
				return "false";
			}
		}
		else{
			return "false";
		}
		
	}	
}
