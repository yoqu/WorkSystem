<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/view/global/html_top.jsp"></jsp:include>

<!-- 内容开始 -->
<div class="am-tab-panel am-fade  am-active am-in" id="tab1">
      	    <form>
      	    <fieldset>
      	    <legend>添加学生</legend>
      	    <div class="am-form-group am-form-icon">
      	    	<i class="am-icon-user"></i>
      	     	<input class="am-form-field am-radius" placeholder="用户名" id="LoginId" name="LoginId" type="text"/>
      	     </div>
      	      <div class="am-form-group am-form-icon">
      	     	<i class="am-icon-lock am-icon-fw"></i>
      	   		  <input class="am-form-field am-radius" id="LoginPwd" placeholder="密码" name="LoginPwd" type="password"/>
      	     </div>
      	     <div class="am-form-group am-form-icon">
      	   		  <i class="am-icon-user"></i>
      	     	<input class="am-form-field am-radius" placeholder="姓名" id="name"name="name" type="text"/>
      	     </div>
      	     <div class="am-form-group am-form-icon">
      	     	 <i class="am-icon-street-view"></i>
      	     	<input class="am-form-field am-radius" placeholder="年龄" id="age" name="age" type="text"/>
      	     </div>
      	      <div class="am-form-group">
         		<label for="doc-ds-ipt-1">系部</label>
		            <select data-am-selected="{btnSize: 'sm'}" id="department" class="department">
		            <option value="${department.did}">${department.name}</option>
		            </select>
		          <label for="doc-ds-ipt-1">专业</label>
		            <select data-am-selected="{btnSize: 'sm'}" id="classes" class="classes">
		            <option value="${classes.cid}">${classes.name}</option>
		            </select>
          	 </div>
				<button type="button" id="studentdoregister" class="am-btn am-btn-warning am-u-lg-12">注册</button>
      	    </fieldset>
			</form>
      </div>
<!-- 内容结束 -->
<jsp:include page="/view/global/html_bottom.jsp"></jsp:include>
<script>
$(document).ready(function(){
	//学生注册监听
	$("#studentdoregister").click(function(){
		var loginId = $("#LoginId").val();
		var loginPwd = $("#LoginPwd").val();
		var name = $("#name").val();
		var age = $("#age").val();
		if($.trim(loginId)==""){
			layer.tips('登陆名不能为空', '#LoginId');
			return false;
		}
		if($.trim(loginPwd)==""){
			layer.tips("密码不能为空",'#LoginPwd');
			return false;
		}
		if($.trim(name)==""){
			layer.tips("姓名不能为空","#name");
			return false;
		}
		if($.trim(age)==""){
			layer.tips("年龄不能为空","#age");
			return false;
		}
		$.ajax({
			url:'/WorkSystem/user/doregisterstudent',	
			beforeSend:function(){
				$.AMUI.progress.start();
			},
			complete:function(){
				 $.AMUI.progress.done();
			},
			//async:false,
			type:"POST",
			data:{
				loginId:$("#LoginId").val(),
				loginPwd:$("#LoginPwd").val(),
				name:$("#name").val(),
				age:$("#age").val(),
				department:$("#department").val(),
				classes:$("#classes").val(),
				},
			error:function(jqXHR,textStatus,errorThrown){
				if(textStatus=="error"){
					layer.msg('内部服务错误！请联系网站管理员');
				}
			},
			success:function(data,status){
				if(data>=100){
					if(data==100){
						layer.msg("用户已注册，不能重复注册");
					}
				}
				else{
					if(data==1){
						layer.msg("注册成功，即将跳转页面");
						//delay(5);
					setTimeout(function(){
						location.href="/WorkSystem/manage/student";
					}, 500);
					}
					else{
						layer.msg("注册失败 未知错误");
					}
				}
			}
		});
	});
	
	
	
});

</script>


