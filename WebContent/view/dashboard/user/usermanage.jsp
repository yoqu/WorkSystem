<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/view/global/html_top.jsp"></jsp:include>


<!-- 内容开始 -->
<div class="am-g">
      <div class="am-u-sm-12 am-u-md-6">
        <div class="am-btn-toolbar">
          <div class="am-btn-group am-btn-group-xs">
            <button type="button" class="am-btn am-btn-default" id="addstudent"><span class="am-icon-plus"></span> 新增</button>
          </div>
        </div>
      </div>
</div>

<!--table-->
<div class="am-g">
<div class="am-u-sm-12">
	<form class="am-form">
		<table class="am-table am-table-striped am-table-hover table-main">
			<thead>
				<tr>
					<th>ID</th>
					<th>姓名</th>
					<th>所属系</th>
					<th>所属专业</th>
					<!-- <th class="table-set">操作</th> -->
				</tr>
			</thead>
			<tbody>
			<c:forEach items="${pagebean.getList()}" var="student">
			  	<tr>
					<td>${student.id}</td>
					<td>${student.user.name}</td>
					<td>${student.department.name}</td>
					<td>${student.classes.name}</td>
					<!-- <td>
	                <div class="am-btn-toolbar">
	                  <div class="am-btn-group am-btn-group-xs">
	                    <button class="am-btn am-btn-default am-btn-xs am-text-secondary" id="edit"><span class="am-icon-pencil-square-o"></span> 编辑</button>
	                    <button class="am-btn am-btn-default am-btn-xs am-text-danger am-hide-sm-only" id="delete" ><span class="am-icon-trash-o"></span> 删除</button>
	                  </div>
	                </div>
             	  </td>
             	   -->
				</tr>
			</c:forEach>
			</tbody>
		</table>
		<div class="am-fr">
<ul class="am-pagination">
			  <li  <c:if test="${pagebean.isHasPreviousPage()==false}">
			  class="am-disabled"
			  </c:if>><a href="?page=${pagebean.getCurrentPage()-1}">&laquo;</a></li>
			  <c:forEach var="item" varStatus="status" begin="0" end="${pagebean.getTotalPage()-1}">
 			 	  <li
 			 	  <c:if test="${pagebean.getCurrentPage()-1 == status.index}">
			  class="am-active"
			  </c:if>
 			 	  ><a href="?page=${status.index+1}">${status.index+1}</a></li>
			  </c:forEach>
			  <li 
			  <c:if test="${pagebean.isHasNextPage()==false}">
			  class="am-disabled"
			  </c:if>
			  ><a href="?page=${pagebean.getCurrentPage()+1}">&raquo;</a></li>
</ul>
  </div>
	</form>	
</div>
</div>


<!-- 内容结束 -->
<jsp:include page="/view/global/html_bottom.jsp"></jsp:include>

<script>
$(document).ready(function(){
	$("#addstudent").click(function(){
		location.href="student/add";
	});
});

</script>


