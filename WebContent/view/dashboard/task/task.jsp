<%@ page language="java" contentType="text/html; charset=UTF-8"
import="com.baidu.ueditor.ActionEnter"
    pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<jsp:include page="/view/global/html_top.jsp"></jsp:include>
<!-- 内容开始 -->
	<div class="node">
		<h3 class="title">${task.title}</h3>
			<div class="author">
						<span class="ou-wp-ct am-icon-user"></span>
						<span class="am-blog-auth">
							${task.user.name}						</span>
						<span class="ou-wp-date">
							${fn:substring(task.time,0,10)}						</span>
						<span class="ou-wp-tags">
													</span>
			</div>
			<hr>
			<div class="content">
				${task.body}
			</div>
			<section data-am-widget="accordion" class="am-accordion am-accordion-basic" data-am-accordion='{   }'>
				<dl class="am-accordion-item am-active">
      			  <dt class="am-accordion-title">
        		  	试卷
     			   </dt>
       			  <dd class="am-accordion-bd am-collapse am-in">
      		    	<div class="am-accordion-content">
      		    		<c:forEach items="${toss}" var="subject">
      		    		<li><a href="${pageContext.request.contextPath}/test/${subject.id}">${subject.name}</a></li>
      		    		</c:forEach>
        			</div>
        		  </dd>
      			</dl>	
			</section>
	</div>
	<div class="comments am-margin-top am-margin-bottom-sm">
		<h3 class="comments-title">
			${task.comment} 条评论</h3>
			<ol class="am-comments-list">
				<c:forEach items="${comments}" var="comment">
				<li class="comment even thread-odd thread-alt depth-1 am-comment" id="li-comment-37">
					<span class="am-comment-avatar"><i class="am-icon-user am-icon-btn"></i></span>
					<article id="comment-37" class="am-comment-main">
						<header class="am-comment-hd am-comment-author vcard">
							<div class="am-comment-meta">
								<cite><b class="am-comment-author">${comment.user.name}</b> </cite><time datetime="${fn:substring(comment.time,0,10)}">${fn:substring(comment.time,0,16)}</time>							</div>
						</header>
						<!-- .comment-meta -->
						<section class="am-comment-bd">
						${comment.body}
						</section>
						<!-- .comment-content -->
					</article>
					<!-- #comment-## -->
				</li>
				</c:forEach>
			</ol>
			<hr>
			<div id="respond" class="comment-respond">
				<h3 id="reply-title" class="comment-reply-title">发表评论 <small><a rel="nofollow" id="cancel-comment-reply-link" href="/archives/247#respond" style="display:none;">取消</a></small></h3>
									<form  id="comment-form" class="comment-form" novalidate="">
										<div id="commentbody"></div>
										<input name="tid" value="${task.tid}"id="taskid" type="hidden"/>
										<button class="am-btn am-btn-primary am-btn-xs" id="btncomment" type="button">评论</button>
									</form>
			</div>
	</div>
<!-- 内容结束 -->
<jsp:include page="/view/global/html_bottom.jsp"></jsp:include>
<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath}/assets/js/ueditor/ueditor.config.js"></script>
<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath}/assets/js/ueditor/ueditor.all.min.js"> </script>    
  <script>
  $(document).ready(function () {
		//alert("开始");
		var ue = UE.getEditor('commentbody',{
           	toolbars: [[
           	             'emotion','bold','fontsize','insertimage'
           	           ]],
           });
		$("#btncomment").click(function(){
			if(ue.getContent()==""){
				layer.msg('回复失败,评论不能为空白');
				return false;
			}
			$.ajax({
				url:'${pageContext.request.contextPath}/dashboard/task/docomment',	
				type:'POST',
				async:false,
				data:{editorValue:ue.getContent(),tid:$("#taskid").val()},
				error:function(jqXHR,textStatus,errorThrown){
					if(textStatus=="error"){
						layer.msg('内部服务错误！请联系网站管理员');
					}
				},
				success:function(data,status){
					if(data!="false"){
						layer.msg("回复成功")
						location.reload();
					}
					else{
						layer.msg('回复失败');
					}
				}
			});
		});
	 });
  </script>