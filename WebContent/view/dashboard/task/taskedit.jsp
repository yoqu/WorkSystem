<%@ page language="java" contentType="text/html; charset=UTF-8"
import="com.baidu.ueditor.ActionEnter"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/view/global/html_top.jsp"></jsp:include>
<link href="${pageContext.request.contextPath}/assets/css/amazeui.datetimepicker.css" rel="stylesheet" type="text/css" />
<!-- 内容开始 -->
     <form class="am-form" title="${task.tid}" id="taskform">
      <fieldset>
      		<legend>修改任务</legend>
            <div class="am-g am-margin-top">
                <div class="am-u-sm-4 am-u-md-2 am-text-right">
                   	任务标题
                </div>
                <div class="am-u-sm-8 am-u-md-4">
                    <input type="text" class="am-input-sm" id="tasktitle" value="${task.title}">
                </div>
                <div class="am-hide-sm-only am-u-md-6">
                    *必填
                </div>
            </div>
            <div class="am-g am-margin-top">
                <div class="am-u-sm-4 am-u-md-2 am-text-right">
                   	任务完成时间：
                </div>
                <div class="am-u-sm-4 am-u-md-4">
              		<div class="am-form-group am-form-icon">
      	   		 		 <i class="am-icon-calendar icon-th"></i>
      	     			<input size="16" type="text" id="limittime" readonly class="form-datetime-lang am-form-field" value="${task.limittime}">
      	   		    </div>
                </div> 
                <div class="am-hide-sm-only am-u-md-6">
                    *必填
                </div>
            </div>
            <div class="am-g am-margin-top">
                <div class="am-u-sm-4 am-u-md-2 am-text-right" >
                    关注任务的人：
                 </div>
                <div class="am-u-sm-4 " id="pokermans">
                	<c:forEach items="${pokerusers}" var="suser" varStatus="status">
  			 			 <span class="am-badge am-badge-success am-round " onclick="clickadd(this)" id="people${status.index}" uid="${suser.uid}" >${suser.name}</span>
			 		</c:forEach> 
                </div>
                <div class="am-u-md-6 ">
                    <button class="am-btn am-btn-default" id="addpokerman" type="button">
 						 <i class="am-icon-plus"></i>
					</button>
                </div>
            </div>
            <div class="am-g am-margin-top-sm">
                <div class="am-u-sm-12 am-u-md-2 am-text-right admin-form-text">
                    内容
                </div>
                <div class="am-u-sm-12 am-u-md-10">
                    <div id="commentbody">
					</div>
                </div>
            </div>
            <div class="am-g">
               <div class="am-u-sm-6"></div>
            	<button type="button" class="am-btn am-btn-primary  am-radius" id="updatetask">发布</button>
            </div>
            </fieldset>
        </form>
 <div id="addpokermanwindow" style="display:none; position:relative">
 	<div class="am-panel am-panel-default">
 		 <div class="am-panel-hd">选择参与人员：</div>
 		 <div class="am-panel-bd" style="position:relative;" id="choosebefore" >
  			 <div class="content">
  			 <c:forEach items="${notpokerusers}" var="suser" varStatus="status">
  			  <span class="am-badge am-badge-success am-round " onclick="clickadd(this)" id="people${status.index}" uid="${suser.uid}" >${suser.name}</span>
			 </c:forEach>  			 
  			  <div class="am-cf"></div>
  			  </div>
 		 </div>
	</div>
 	<hr>
 	<div class="am-panel am-panel-default">
 			 <div class="am-panel-hd">已关注任务列表</div>
 			 <div class="am-panel-bd" id="chooseafter" style="position:relative;">
			 <c:forEach items="${pokerusers}" var="suser" varStatus="status">
  			  <span class="am-badge am-badge-success am-round " onclick="clickadd(this)" id="people${status.index}" uid="${suser.uid}" >${suser.name}</span>
			 </c:forEach>  	
				<div class="am-cf"></div>
 			 </div>
	</div>
 </div>
<!-- 内容结束 -->
<jsp:include page="/view/global/html_bottom.jsp"></jsp:include>
<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath}/assets/js/amazeui.datetimepicker.js"></script>
<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath}/assets/js/ueditor/ueditor.config.js"></script>
<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath}/assets/js/ueditor/ueditor.all.min.js"> </script>    
<script id="editor" type="text/plain" style="width:100%;"></script>
<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath}/assets/js/task/taskmanage.js"> </script>
<script>
$(document).ready(function () {
	var ue = UE.getEditor('commentbody');
	ue.ready(function() {
	    //设置编辑器的内容
	    var taskbody="${task.body}";
	    ue.setContent(taskbody);
	});
});
</script>    
