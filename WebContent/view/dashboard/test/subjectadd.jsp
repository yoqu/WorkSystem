<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/view/global/html_top.jsp"></jsp:include>
<!-- 内容开始 -->
<form class="am-form">
      <fieldset>
      		<legend>添加试卷</legend>
            <div class="am-g am-margin-top">
                <div class="am-u-sm-4 am-u-md-2 am-text-right">
                   	试卷标题
                </div>
                <div class="am-u-sm-8 am-u-md-4">
                    <input type="text" class="am-input-sm" id="subjectname">
                </div>
                <div class="am-hide-sm-only am-u-md-6">
                    *必填
                </div>
            </div>    
            <div class="am-g am-margin-top">
           
                <div class="am-u-sm-4 am-u-md-2 am-text-right">
                   	所属专业
                </div>
                <div class="am-u-sm-4 am-u-md-4">
              		<select multiple data-am-selected id="classes">
		  			 	 <option value="${classes.cid }">${classes.name}</option>
					</select>
                </div> 
                <div class="am-hide-sm-only am-u-md-6">
                    *必填
                </div>
            </div> 
            <div class="am-g am-margin-top">
                <div class="am-u-sm-4 am-u-md-2 am-text-right">
                   	专业学分
                </div>
                <div class="am-u-sm-8 am-u-md-4">
                    <input type="text" class="am-input-sm" id="subjectcredit">
                </div>
                <div class="am-hide-sm-only am-u-md-6">
                    *必填
                </div>
            </div>
             <div class="am-checkbox">
    			 <label>
   					   <input type="checkbox" name="checksave" id="checksave">保存并添加试题
   				 </label>
  			</div>
             <div class="am-g">
               <div class="am-u-sm-6"></div>
            	<button type="button" class="am-btn am-btn-primary  am-radius" id="postsubject">保存</button>
            </div>    		
      </fieldset>
</form>
<!-- 内容结束 -->
<jsp:include page="/view/global/html_bottom.jsp"></jsp:include>
<script  src="${pageContext.request.contextPath}/assets/js/test/subjectadd.js"></script>
<script>

</script>