<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/view/global/html_top.jsp"></jsp:include>
<!-- 内容开始 -->
<form class="am-form">
      <fieldset>
      		<legend>修改试卷</legend>
            <div class="am-g am-margin-top">
                <div class="am-u-sm-4 am-u-md-2 am-text-right">
                   	试卷标题
                </div>
                <div class="am-u-sm-8 am-u-md-4">
                    <input type="text" class="am-input-sm" id="subjectname" value="${subject.name }">
                </div>
                <div class="am-hide-sm-only am-u-md-6">
                    *必填
                </div>
            </div>    
            <div class="am-g am-margin-top">
                <div class="am-u-sm-4 am-u-md-2 am-text-right">
                   	所属专业
                </div>
                <div class="am-u-sm-4 am-u-md-4">
              		<select multiple data-am-selected id="classes">
		  			 	 <option value="${subject.classes.cid }" selected="selected">${subject.classes.name}</option>
					</select>
                </div> 
                <div class="am-hide-sm-only am-u-md-6">
                    *必填
                </div>
            </div> 
            <div class="am-g am-margin-top">
                <div class="am-u-sm-4 am-u-md-2 am-text-right">
                   	专业学分
                </div>
                <div class="am-u-sm-8 am-u-md-4">
                    <input type="text" class="am-input-sm" id="subjectcredit" value="${subject.credit}">
                </div>
                <div class="am-hide-sm-only am-u-md-6">
                    *必填
                </div>
            </div>
             <div class="am-g">
               <div class="am-u-sm-6"></div>
            	<button type="button" class="am-btn am-btn-primary  am-radius" id="postsubject">保存</button>
            </div>    		
      </fieldset>
</form>
  <hr data-am-widget="divider" style="" class="am-divider am-divider-dashed" />
  <div class="am-g">
  <div class="am-u-sm-12 am-u-md-6">
        <div class="am-btn-toolbar">
          <div class="am-btn-group am-btn-group-xs">
            <button type="button" class="am-btn am-btn-default" id="addtestbutton"><span class="am-icon-plus"></span> 新增</button>
          </div>
        </div>
      </div>
  </div>
<table class="am-table am-table-bordered am-table-radius am-table-striped">
	<caption>试题</caption>
    <thead>
        <tr>
            <th>题目ID</th>
            <th>题目</th>
            <th>答案</th>
            <th>正确答案</th>
            <th>操作</th>
        </tr>
    </thead>
    <tbody>
    	<c:forEach items="${tests}" var="test">
    	<tr class="test${test.id}">
            <td>${test.id}</td>
            <td class="content">${test.content}</td>
            <td>
 				<c:forEach items="${test.answers}" var="answer">
 				 <p   class="allanswer${answer.id} allanswer">
            		 ${answer.content} 
            	</p>
            	</c:forEach>
			</td>
            <td>
          			  <p class="correntanswer" correntaid="${test.answerid}"></p>
            </td>
            <td>
            	<div class="am-dropdown" data-am-dropdown="">
                <button class="am-btn am-btn-default am-btn-xs am-dropdown-toggle" data-am-dropdown-toggle=""><span class="am-icon-cog"></span> <span class="am-icon-caret-down"></span></button>
                <ul class="am-dropdown-content">
                  <li><a href="#" onclick="loadti(${test.id})">编辑</a></li>
                  <li><a href="#" onclick="deletetest(${test.id})">删除</a></li>
                </ul>
              </div>
            </td>
        </tr>
    	</c:forEach>
    </tbody>
</table>
<hr>
<hr><hr><hr>
<form class="timuwindow" style="display: none;">
	<div class="am-g am-margin-top">
                <div class="am-u-sm-4 am-u-md-2 am-text-right">
                   	题目
                </div>
                <div class="am-u-sm-8 am-u-md-4">
                     <textarea class rows="5" id="titext" style="width: 250px;"name="text"	></textarea>
                </div>
                <div class="am-hide-sm-only am-u-md-6">
                    *必填
                </div>
            </div>    
            <div class="am-g am-margin-top">
            		<div class="am-u-sm-4 am-u-md-2 am-text-right" ><input type="radio" name="selectanswer" value="0"/>A</div>
            		<div class="am-u-sm-8 am-u-md-4" style="float: left;">
            			<input type="text" id="answerA" class="am-input-sm"  style="width:250px;"/>
            		</div>
            	</div>
            	<div class="am-g am-margin-top">
            		<div class="am-u-sm-4 am-u-md-2 am-text-right" ><input type="radio" name="selectanswer" value="1" />B
            		</div>
            		<div class="am-u-sm-8 am-u-md-4" style="float: left;">
            			<input type="text" id="answerB" class="am-input-sm" style="width:250px;"/>
            		</div>
            	</div>
            	<div class="am-g am-margin-top">
            		<div class="am-u-sm-4 am-u-md-2 am-text-right"><input type="radio" name="selectanswer" value="2"/>C</div>
            		<div class="am-u-sm-8 am-u-md-4" style="float: left;">
            			<input type="text" id="answerC" class="am-input-sm" style="width:250px;"/>
            		</div>
            	</div>
            	<div class="am-g am-margin-top" id="D">
            		<div class="am-u-sm-4 am-u-md-2 am-text-right"><input type="radio" name="selectanswer" value="3" />D</div>
            		<div class="am-u-sm-8 am-u-md-4" style="float: left;">
            			<input type="text" id="answerD" class="am-input-sm" style="width:250px;"/>
            		</div>
            	</div>
</form>
<!-- 内容结束 -->
<jsp:include page="/view/global/html_bottom.jsp"></jsp:include>

<script>
 $(document).ready(function () {
 		$(".correntanswer").each(function(index){
 			var correntanswerId=$(this).attr("correntaid");
 			$(this).text($(".allanswer"+correntanswerId).text());
 		});
	 	$("#addtestbutton").click(function(){
	 		loadti(0);
	 	});
	 	
	 	$("#postsubject").click(function(){
	 		savesubject();
	 	});
 });
 function loadti(tid){
	 var flagnew=true;
	 if(tid>0){
		 flagnew=false;
	 	 $("#titext").val($(".test"+tid).find(".content").text());
	 	 $(".test"+tid).find(".allanswer").each(function(index){
	 		 if(index==0)
	 			 $("#answerA").val($.trim($(this).text()));
	 		 if(index==1)
	 			 $("#answerB").val($.trim($(this).text()));
	 		 if(index==2)
	 			 $("#answerC").val($.trim($(this).text()));
	 		 if(index==3)
	 			 $("#answerD").val($.trim($(this).text()));
	 	 });
	 }
 	 var html=$(".timuwindow").css("display","block");
 	 layer.open({
			    type: 1,
			    skin: 'layui-layer-rim', //加上边框
			    area: ['620px', '440px'], //宽高
			    content: html,
			    btn:['确认'],
			    yes:function(index,layero){
			    	
			    	 if(flagnew){
			    	 	 savenewtest();
			    	 }
			    	 else
			    		 saveedittest(tid);
			    		 layer.close(index);
			    },
			});
 }
 
 function savenewtest(){
 	var content=$("#titext").val();
 	if(content==""){
 		layer.msg("请输入问题内容");
 		return false;
 	}
 	var correctanswer = $("input[name='selectanswer']:checked").val();
 	if(correctanswer==null){
 		layer.msg("必须选择一个选项");
 		return false;
 	}
 	var answera=$("#answerA").val();
 	var answerb=$("#answerB").val();
 	var answerc=$("#answerC").val();
 	var answerd=$("#answerD").val();
 	if(answera=="" ||answerb=="" || answerc=="" || answerd==""){
 		layer.msg("必须输入每一个问题");
 		return false;
 	}
	 $.ajax({
	 	type:"post",
	 	url:"/WorkSystem/manage/test/doaddtest",
	 	async:false,
		beforeSend:function(){
					$.AMUI.progress.start();
		},
		complete:function(){
					 $.AMUI.progress.done();
		},
		data:{
					content:$("#titext").val(),
					grade:3,
					sid:${subject.id},
					contenta:answera,
					contentb:answerb,
					contentc:answerc,
					contentd:answerd,
					correctanswer:correctanswer,
		},
		error:function(jqXHR,textStatus,errorThrown){
			if(textStatus=="error"){
				layer.msg('内部服务错误！请联系网站管理员');
			}
		},
		success:function(data,status){
			if(data==1){
					layer.msg("添加成功")
					setTimeout(function(){
							location.reload();
					},1000);
					layer.close(0);
				}
				else{
					layer.msg('添加失败');
				}
			}
	 });
 }
 function saveedittest(tid){
	 var content=$("#titext").val();
 	if(content==""){
 		layer.msg("请输入问题内容");
 		return false;
 	}
 	var correctanswer = $("input[name='selectanswer']:checked").val();
 	if(correctanswer==null){
 		layer.msg("必须选择一个选项");
 		return false;
 	}
 	var answera=$.trim($("#answerA").val());
 	var answerb=$.trim($("#answerB").val());
 	var answerc=$.trim($("#answerC").val());
 	var answerd=$.trim($("#answerD").val());
 	if(answera=="" ||answerb=="" || answerc=="" || answerd==""){
 		layer.msg("必须输入每一个问题");
 		return false;
 	}
	 $.ajax({
	 	type:"post",
	 	url:"/WorkSystem/manage/test/doedittest",
	 	async:false,
		beforeSend:function(){
					$.AMUI.progress.start();
		},
		complete:function(){
					 $.AMUI.progress.done();
		},
		data:{
					tid:tid,
					content:$("#titext").val(),
					contenta:answera,
					contentb:answerb,
					contentc:answerc,
					contentd:answerd,
					correctanswer:correctanswer,
		},
		error:function(jqXHR,textStatus,errorThrown){
			if(textStatus=="error"){
				layer.msg('内部服务错误！请联系网站管理员');
			}
		},
		success:function(data,status){
			if(data==1){
					layer.msg("修改成功")
					setTimeout(function(){
							location.reload();
					},1000);
					layer.close(0);
				}
				else{
					layer.msg('修改失败');
				}
			}
	 });
 }

function deletetest(tid){
	if(tid<=0 || tid==null){
		layer.msg("参数异常,删除失败");
		return false;
	}
	 $.ajax({
	 	type:"post",
	 	url:"/WorkSystem/manage/test/dotestdelete",
	 	async:false,
		beforeSend:function(){
					$.AMUI.progress.start();
		},
		complete:function(){
					 $.AMUI.progress.done();
		},
		data:{
					tid:tid,
		},
		error:function(jqXHR,textStatus,errorThrown){
			if(textStatus=="error"){
				layer.msg('内部服务错误！请联系网站管理员');
			}
		},
		success:function(data,status){
			if(data==1){
					layer.msg("删除成功")
					setTimeout(function(){
							location.reload();
					},1000);
					layer.close(0);
				}
				else{
					layer.msg('删除失败');
				}
			}
	 });
}

function savesubject(){
	var subjectname=$("#subjectname").val();
			if($.trim(subjectname)==""){
				alert("标题不能为空");
				return false;
			}
		var classesid=$("#classes  option:selected").val();
			if(classesid==""){
				alert("专业选项不能为空");
				return false;
			}
		var credit = $("#subjectcredit").val();
			if(credit==""){
				alert("专业学分不能为空");
				return false;
			}
		$.ajax({
				url:'/WorkSystem/manage/test/doeditsubject',	
				async:false,
				type:"POST",
				beforeSend:function(){
					$.AMUI.progress.start();
				},
				complete:function(){
					 $.AMUI.progress.done();
				},
				dataType:"json",
				data:{
					sid:${subject.id},
					name:subjectname,
					classesid:classesid,
					credit:credit,
					},
				error:function(jqXHR,textStatus,errorThrown){
					if(textStatus=="error"){
						layer.msg('内部服务错误！请联系网站管理员');
					}
				},
				success:function(data,status){
					if(data!="-1"){
						layer.msg('修改试卷成功');
						location.reload();
					}
					else{
						layer.msg('修改试卷失败');
					}
					
				},
			})
}
</script>