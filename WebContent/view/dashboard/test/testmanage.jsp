<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/view/global/html_top.jsp"></jsp:include>
<!-- 导航开始 -->
	  <div class="am-collapse am-topbar-collapse" id="doc-topbar-collapse-3">
            <ul class="am-nav am-nav-pills am-topbar-nav">
                <li>
                    <a href="${pageContext.request.contextPath}/manage/test/subjectadd">添加试卷</a>
                </li>
            </ul>
        </div>	
<!-- 导航结束 -->
<!-- 内容开始 -->
 <ul class="am-list am-list-static am-list-border">
  		<c:forEach var="subject" items="${pagebean.getList()}">
            <li class="${subject.id}">
            	<i class="am-icon-book"></i>
               	<span class="title" >${subject.name}  </span>
                <div class="am-fr">
               	<button type="button" class="am-btn-xs am-btn am-btn-success" onclick="showtaskdetail(${subject.id})">查看</button>
               	<button type="button" class="am-btn-xs am-btn am-btn-danger deletetest" test="${subject.id}">删除</button>
               </div>
               <span class="am-cf"></span>
            </li>
         </c:forEach>
</ul>
<ul class="am-pagination">
			  <li  <c:if test="${pagebean.isHasPreviousPage()==false}">
			  class="am-disabled"
			  </c:if>><a href="?page=${pagebean.getCurrentPage()-2}">&laquo;</a></li>
			  <c:forEach var="item" varStatus="status" begin="0" end="${pagebean.getTotalPage()-1}">
 			 	  <li
 			 	  <c:if test="${pagebean.getCurrentPage()-1 == status.index}">
			  class="am-active"
			  </c:if>
 			 	  ><a href="?page=${status.index}">${status.index+1}</a></li>
			  </c:forEach>
			  <li 
			  <c:if test="${pagebean.isHasNextPage()==false}">
			  class="am-disabled"
			  </c:if>
			  ><a href="?page=${pagebean.getCurrentPage()}">&raquo;</a></li>
			</ul>
<!-- 内容结束 -->
<jsp:include page="/view/global/html_bottom.jsp"></jsp:include>
<script  src="${pageContext.request.contextPath}/assets/js/test/testlist.js"></script>
<script>
function showtaskdetail(tid){
	location.href="${pageContext.request.contextPath}/manage/test/edit/"+tid;
}

</script>