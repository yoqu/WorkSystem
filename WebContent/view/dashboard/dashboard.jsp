<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/view/global/html_top.jsp"></jsp:include>
	<div class="am-g">
        <div class="am-panel am-panel-default">
          <div class="am-panel-hd am-cf" data-am-collapse="{target: '#collapse-panel-4'}">我的近期任务<span class="am-icon-chevron-down am-fr"></span></div>
          <div id="collapse-panel-4" class="am-panel-bd am-collapse am-in">
            <ul class="am-list admin-content-task">
              <c:forEach var="task" items="${pagebean.getList()}">
              <li>
                <div class="admin-task-meta">编辑时间：${task.time}  编辑:${task.user.name}</div>
                <div class="admin-task-bd">
                  ${task.title}
                </div>
                <div class="am-cf">
                  <div class="am-btn-toolbar am-fl">
                    	<span class="am-icon-user"></span> &nbsp;<span class="am-badge am-badge-success">${task.comment }</span>
                  </div>
                  <div class="am-fr">
                    <button class="am-btn am-btn-default am-btn-xs am-text-secondary" onclick="viewtask(${task.tid})"><span class="am-icon-pencil-square-o"></span>查看详情</button>
                  </div>
                </div>
              </li>
              </c:forEach>
            </ul>
            <ul class="am-pagination">
			  <li  <c:if test="${pagebean.isHasPreviousPage()==false}">
			  class="am-disabled"
			  </c:if>><a href="?page=${pagebean.getCurrentPage()-1}">&laquo;</a></li>
			  <c:forEach var="item" varStatus="status" begin="0" end="${pagebean.getTotalPage()-1}">
 			 	  <li
 			 	  <c:if test="${pagebean.getCurrentPage()-1== status.index}">
			  class="am-active"
			  </c:if>
 			 	  ><a href="?page=${status.index}">${status.index+1}</a></li>
			  </c:forEach>
			  <li 
			  <c:if test="${pagebean.isHasNextPage()==false}">
			  class="am-disabled"
			  </c:if>
			  ><a href="?page=${pagebean.getCurrentPage()}">&raquo;</a></li>
			</ul>
          </div>
          </div>
          </div>
<jsp:include page="/view/global/html_bottom.jsp"></jsp:include>
<script>
function viewtask(tid){
	location.href="${pageContext.request.contextPath}/dashboard/task/"+tid;
}
</script>