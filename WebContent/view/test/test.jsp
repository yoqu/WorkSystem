<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/view/global/html_top.jsp"></jsp:include>
<!-- 内容开始 -->
<link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/styles.css" />
<style type="text/css">
.demo{width:760px; margin:60px auto 10px auto}
</style>
<div id="main">
   <h2 class="top_title" style="margin-left: 300px; font-family: '微软雅黑'; margin-top: 10px;">java 程序设计</h2>
			<div class="demo">
				<div id='quiz-container' subjectid="${subject.id}"></div>
			</div>
</div>
<!-- 内容结束 -->
<jsp:include page="/view/global/html_bottom.jsp"></jsp:include>
<script  src="${pageContext.request.contextPath}/assets/js/task/tasklist.js"></script>
<script  src="${pageContext.request.contextPath}/assets/js/test/quiz.js" ></script>
<script>
var init=${tests};
	 $(function () {
	        $('#quiz-container').jquizzy({
	            questions: init
	        });
	    });
</script>
