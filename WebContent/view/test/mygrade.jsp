<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/view/global/html_top.jsp"></jsp:include>
<!-- 内容开始 -->
   <table class="am-table">
    <thead>
        <tr>
            <th>考试科目Id</th>
            <th>考生姓名</th>
            <th>专业</th>
            <th>系</th>
            <th>考试科目名称</th>
            <th>考试成绩</th>
        </tr>
    </thead>
    <tbody>
    <c:forEach items="${grades}" var="grade">
   		<tr>
            <td>${grade.id}</td>
            <td>${user.getName()}</td>
            <td>${grade.student.classes.name}</td>
            <td>${grade.student.department.name}</td>
            <td>${grade.subject.name}</td>
            <td>${grade.score}</td>
        </tr>
    </c:forEach>
    </tbody>
</table>
<!-- 内容结束 -->
<jsp:include page="/view/global/html_bottom.jsp"></jsp:include>