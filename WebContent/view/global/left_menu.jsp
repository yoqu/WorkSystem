<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<div class="am-offcanvas-bar admin-offcanvas-bar">
      <ul class="am-list admin-sidebar-list">
        <li><a href="/WorkSystem/dashboard"><span class="am-icon-home"></span> 首页</a></li>
		<li><a href="/WorkSystem/manage/task/tasklist"><span class="am-icon-home"></span> 任务管理</a></li>
        <li><a href="/WorkSystem/manage/test/testlist"><span class="am-icon-home"></span> 试卷管理</a></li>
        <li><a href="/WorkSystem/user"><span class="am-icon-user"></span>个人资料</a></li>
        <li><a href="/WorkSystem/user/logout"><span class="am-icon-sign-out"></span> 注销</a></li>
      </ul>
    </div>