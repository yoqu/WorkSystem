<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>欢迎登录后台管理系统</title>
<link href="${pageContext.request.contextPath}/assets/css/amazeui.css" rel="stylesheet" type="text/css" />
<script  src="${pageContext.request.contextPath}/assets/js/jquery.min.js"></script>
<script  src="${pageContext.request.contextPath}/assets/js/layer.js"></script>
<script  src="${pageContext.request.contextPath}/assets/js/user/register.js"></script>
<script  src="${pageContext.request.contextPath}/assets/js/amazeui.min.js"></script>
<script  src="${pageContext.request.contextPath}/assets/js/app.js"></script>
<style>
.header{
	text-align: center;
}
.header h1{
	margin-top: 25px;
}

</style>
</head>

<body>
<div class="header">
  <div class="am-g">
    <h1>在线作业分发系统注册</h1>
  </div>
  <hr>
</div>
<div class="am-g">
<div class="am-u-lg-6 am-u-md-8 am-u-sm-centered">


<div class="am-tabs am-margin" data-am-tabs="">
    <ul class="am-tabs-nav am-nav am-nav-tabs">
      <li class="am-active"><a href="#tab1">学生注册</a></li>
      <li ><a href="#tab2">老师注册</a></li>
    </ul>
    <div class="am-tabs-bd" style="touch-action: pan-y; -webkit-user-select: none; -webkit-user-drag: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
      <div class="am-tab-panel am-fade  am-active am-in" id="tab1">
      	    <form>
      	    <fieldset>
      	    <legend>学生注册</legend>
      	    <div class="am-form-group am-form-icon">
      	    	<i class="am-icon-user"></i>
      	     	<input class="am-form-field am-radius" placeholder="用户名" id="LoginId" name="LoginId" type="text"/>
      	     </div>
      	      <div class="am-form-group am-form-icon">
      	     	<i class="am-icon-lock am-icon-fw"></i>
      	   		  <input class="am-form-field am-radius" id="LoginPwd" placeholder="密码" name="LoginPwd" type="password"/>
      	     </div>
      	     <div class="am-form-group am-form-icon">
      	   		  <i class="am-icon-user"></i>
      	     	<input class="am-form-field am-radius" placeholder="姓名" id="name"name="name" type="text"/>
      	     </div>
      	     <div class="am-form-group am-form-icon">
      	     	 <i class="am-icon-street-view"></i>
      	     	<input class="am-form-field am-radius" placeholder="年龄" id="age" name="age" type="text"/>
      	     </div>
      	      <div class="am-form-group">
         		<label for="doc-ds-ipt-1">系部</label>
		            <select data-am-selected="{btnSize: 'sm'}" id="department" class="department">
		            </select>
		          <label for="doc-ds-ipt-1">班级</label>
		            <select data-am-selected="{btnSize: 'sm'}" id="classes" class="classes">
		            </select>
          	 </div>
				<button type="button" id="studentdoregister" class="am-btn am-btn-warning am-u-lg-12">注册</button>
      	    </fieldset>
			</form>
      </div>
      <div class="am-tab-panel am-fade" id="tab2">
      			  <form>
      	    <fieldset>
      	    <legend>教师注册</legend>
      	    <div class="am-form-group am-form-icon">
      	    	<i class="am-icon-user"></i>
      	     	<input class="am-form-field am-radius" placeholder="用户名" id="teaLoginId" name="teaLoginId" type="text"/>
      	     </div>
      	      <div class="am-form-group am-form-icon">
      	     	<i class="am-icon-lock am-icon-fw"></i>
      	   		  <input class="am-form-field am-radius" id="teaLoginPwd" placeholder="密码" name="teaLoginPwd" type="password"/>
      	     </div>
      	     <div class="am-form-group am-form-icon">
      	   		  <i class="am-icon-user"></i>
      	     	<input class="am-form-field am-radius" placeholder="姓名" id="teaname"name="name" type="text"/>
      	     </div>
      	     <div class="am-form-group am-form-icon">
      	     	 <i class="am-icon-street-view"></i>
      	     	<input class="am-form-field am-radius" placeholder="年龄" id="teaage" name="age" type="text"/>
      	     </div>
      	     <div class="am-form-group am-form-icon">
      	     	<i class="am-icon-bullhorn"></i>
      	     	<input class="am-form-field am-radius" placeholder="职称" id="teatitle" name="age" type="text"/>
      	     </div>
      	      <div class="am-form-group">
         		<label for="doc-ds-ipt-1">系部</label>
		            <select data-am-selected="{btnSize: 'sm'}" id="teadepartment" class="department">
		            </select>
		          <label for="doc-ds-ipt-1">班级</label>
		            <select data-am-selected="{btnSize: 'sm'}" id="teaclasses" class="classes">
		            </select>
          	 </div>
				<button type="button" id="teacherdoregister" class="am-btn am-btn-warning am-u-lg-12">注册</button>
      	    </fieldset>
			</form>
      </div>
    </div>
  </div>
</div>
</div>
</body>
</html>
