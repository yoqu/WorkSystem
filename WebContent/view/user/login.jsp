<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>欢迎登录后台管理系统</title>
<link href="${pageContext.request.contextPath}/assets/css/amazeui.css" rel="stylesheet" type="text/css" />
<script  src="${pageContext.request.contextPath}/assets/js/jquery.js"></script>
<script  src="${pageContext.request.contextPath}/assets/js/layer.js"></script>

<script>
$(document).ready(function () {
	//alert("开始");
	$("#register").click(function(){
		location.href="${pageContext.request.contextPath}/user/register";
	});	
	
	$("#dologin").click(function(){
		$.ajax({
			url:'/WorkSystem/user/dologin',
			beforeSend:function(){
				$.AMUI.progress.start();
			},
			complete:function(){
				 $.AMUI.progress.done();
			},
			async:false,
			data:{loginId:$("#LoginId").val(),loginPwd:$("#LoginPwd").val()},
			error:function(jqXHR,textStatus,errorThrown){
				if(textStatus=="error"){
					layer.msg('内部服务错误！请联系网站管理员');
				}
			},
			success:function(data,status){
				if(data!="false"){
					location.href="${pageContext.request.contextPath}/dashboard";
				}
				else{
					layer.msg('账号密码错误');
				}
			}
		});
	});
 });

</script>
<style>
.header{
	text-align: center;
}
.header h1{
	margin-top: 25px;
}

</style>
</head>

<body>
<div class="header">
  <div class="am-g">
    <h1>在线考试系统登陆</h1>
  </div>
  <hr>
</div>
<div class="am-g">
<div class="am-u-lg-6 am-u-md-8 am-u-sm-centered">
<form method="post" class="am-form">
      <label for="text">账号:</label>
      <input type="text" name="LoginId" id="LoginId"/>
      <br>
      <label for="password">密码:</label>
      <input type="password" name="LoginPwd" id="LoginPwd" />
      <br>
      <div class="am-cf">
        <button type="button" id="dologin" name="dologin" class="am-btn am-btn-primary am-btn-sm am-fl">登陆</button>
        <button type="button" id="register" class="am-btn am-btn-default am-btn-sm am-fr" >注册</button>
      </div>
    </form>
</div>
</div>
	<script  src="${pageContext.request.contextPath}/assets/js/jquery.min.js"></script>
	<script  src="${pageContext.request.contextPath}/assets/js/layer.js"></script>
	<script  src="${pageContext.request.contextPath}/assets/js/amazeui.min.js"></script>
	<script  src="${pageContext.request.contextPath}/assets/js/app.js"></script>

</body>
</html>
