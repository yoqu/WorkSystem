<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/view/global/html_top.jsp"></jsp:include>
<!-- 内容开始 -->
 <div class="am-g">
 	 <div class="am-u-sm-12 am-u-md-8">
        <form class="am-form am-form-horizontal">
          <div class="am-form-group">
            <label for="user-name" class="am-u-sm-3 am-form-label">姓名</label>
            <div class="am-u-sm-9">
              <input type="text" id="user-name" placeholder="姓名/ Name" value="${user.getName()}">
            </div>
          </div>
          <div class="am-form-group">
            <label for="user-loginId" class="am-u-sm-3 am-form-label">用户名</label>
            <div class="am-u-sm-9">
              <input type="text" id="user-loginId" placeholder="用户名" disabled value="${user.getLoginId()}">
            </div>
          </div>
          <div class="am-form-group">
            <label for="user-oldpwd" class="am-u-sm-3 am-form-label">旧密码</label>
            <div class="am-u-sm-9">
              <input type="password" id="user-oldpwd" placeholder="输入旧密码">
            </div>
          </div>
          <div class="am-form-group">
            <label for="user-newpwd" class="am-u-sm-3 am-form-label">新密码</label>
            <div class="am-u-sm-9">
              <input type="password" id="user-newpwd" placeholder="输入你的新密码">
            </div>
          </div>
          <div class="am-form-group">
            <label for="user-againpwd" class="am-u-sm-3 am-form-label">重复新密码</label>
            <div class="am-u-sm-9">
              <input type="password" id="user-againpwd" placeholder="输入以确认你的新密码">
            </div>
          </div>
          <div class="am-form-group">
            <div class="am-u-sm-9 am-u-sm-push-3">
              <button type="button" class="am-btn am-btn-primary" onclick="save()">保存修改</button>
            </div>
          </div>
        </form>
      </div>
 </div>
<!-- 内容结束 -->
<jsp:include page="/view/global/html_bottom.jsp"></jsp:include>
<script  src="${pageContext.request.contextPath}/assets/js/user/changprofile.js"></script>