 $(document).ready(function () {
	 postsubject();
 });
 
 
 function postsubject(){
	 $("#postsubject").click(function(){
			var title=$("#subjectname").val();
			if($.trim(title)==""){
				alert("标题不能为空");
				return false;
			}
			var classesid=$("#classes  option:selected").val();
			if(classesid==""){
				alert("班级选项不能为空");
				return false;
			}
			var credit = $("#subjectcredit").val();
			if(credit==""){
				alert("专业学分不能为空");
				return false;
			}
			var checkstatus = $("input[name='checksave']").is(":checked");;
			$.ajax({
				url:'/WorkSystem/manage/test/doaddsubject',	
				async:false,
				type:"POST",
				beforeSend:function(){
					$.AMUI.progress.start();
				},
				complete:function(){
					 $.AMUI.progress.done();
				},
				dataType:"json",
				data:{
					name:title,
					classesid:classesid,
					credit:credit,
					},
				error:function(jqXHR,textStatus,errorThrown){
					if(textStatus=="error"){
						layer.msg('内部服务错误！请联系网站管理员');
					}
				},
				success:function(data,status){
					if(data!="false"){
						layer.msg('保存试卷成功');
						//如果要跳转到添加试题页面
						if(checkstatus){
							setTimeout(function(){
								location.href='/WorkSystem/manage/test/edit/'+data[0].id;
							}, 1000);
						}
					}
					else{
						layer.msg('保存试卷失败');
					}
					
				},
			})
	});
 }