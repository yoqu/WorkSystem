 $(document).ready(function () {
	 deletetest();
 });
 
 
 function deletetest(){
	 $(".deletetest").click(function(){
		var sid=$(this).attr("test");
		 $.ajax({
				url:'/WorkSystem/manage/test/dosubjectdelete',	
				type:'POST',
				async:false,
				beforeSend:function(){
					$.AMUI.progress.start();
				},
				complete:function(){
					 $.AMUI.progress.done();
				},
				data:{
					sid:sid
					},
				error:function(jqXHR,textStatus,errorThrown){
					if(textStatus=="error"){
						layer.msg('内部服务错误！请联系网站管理员');
					}
				},
				success:function(data,status){
					if(data==1){
						layer.msg("删除成功")
						setTimeout(function(){
							location.reload();
						},1000);
					}
					else{
						layer.msg('删除失败');
					}
				}
			});		 
	 });
 }