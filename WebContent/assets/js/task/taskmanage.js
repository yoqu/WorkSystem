 $(document).ready(function () {
    		//添加日期选择事件监听
	 		addtimeevent();
	 		//加载添加关注人的窗口
    	   loadaddpokermanwindow();
    	   var ue = UE.getEditor('commentbody',{
           	toolbars: [[
           	             'emotion','bold','fontsize','insertimage'
           	           ]],
           });
    	   //添加提交任务事件
    	   posttask(ue);
    	   
    	   //更新任务事件
    	   updatetask(ue);
});
    
 	function posttask(ue){
 		$("#posttask").click(function(){
 				//alert("提交");
 				var title=$("#tasktitle").val();
 				if($.trim(title)==""){
 					alert("标题不能为空");
 					return false;
 				}
 				var time=$("#limittime").val();
 				if($.trim(time)==""){
 					alert("时间不能为空");
 					return false;
 				}
 				var subids=new Array();
 				$("#subjects .am-selected-list .am-checked").each(function(index,checked){
 					var subid=$(this).attr("data-value");
 					subids.push(subid);
 				});
 				var subsjson = JSON.stringify(subids);
 				var uids=new Array();
 				$("#pokermans span").each(function(index,man){
 					var uid=$(this).attr("uid");
 					uids.push(uid);
 				});
 				if(uids==null){
 					alert("必须添加学生参加任务");
 					return false;
 				}
 				var uidsjson = JSON.stringify(uids);
 				var body=ue.getContent();
 				if($.trim(body)==""){
 					alert("必须写任务内容");
 					return false;
 				}
 				postdatatojava(title,time, body, uidsjson,subsjson);
 		});
 	}
 	
 	function updatetask(ue){
 		$("#updatetask").click(function(){
 			//alert("asdadsasda");
 				var tid=$("#taskform").attr("title");
 				var title=$("#tasktitle").val();
 				if($.trim(title)==""){
 					alert("标题不能为空");
 					return false;
 				}
 				var time=$("#limittime").val();
 				if($.trim(time)==""){
 					alert("时间不能为空");
 					return false;
 				}
 				var uids=new Array();
 				$("#pokermans span").each(function(index,man){
 					var uid=$(this).attr("uid");
 					uids.push(uid);
 				});
 				if(uids==null){
 					alert("必须添加学生参加任务");
 					return false;
 				}
 				var uidsjson = JSON.stringify(uids);
 				var body=ue.getContent();
 				if($.trim(body)==""){
 					alert("必须写任务内容");
 					return false;
 				}
 				updatedatatojava(tid,title,time, body, uidsjson);
 		});
 	}
 	function updatedatatojava(tid,title,time,body,uidsjson){
 		$.ajax({
			url:'/WorkSystem/manage/task/dotaskupdate',	
			type:'POST',
			async:false,
			beforeSend:function(){
				$.AMUI.progress.start();
			},
			complete:function(){
				 $.AMUI.progress.done();
			},
			data:{
				tid:tid,
				title:title,
				time:time,
				body:body,
				uids:uidsjson,
				},
			error:function(jqXHR,textStatus,errorThrown){
				if(textStatus=="error"){
					layer.msg('内部服务错误！请联系网站管理员');
				}
			},
			success:function(data,status){
				if(data==1){
					layer.msg("更新成功")
					setTimeout(function(){
						location.href="/WorkSystem/manage/task/tasklist";
					}, 1000);
				}
				else{
					layer.msg('更新失败');
				}
			}
		});
 	}
 	function postdatatojava(title,time,body,uidsjson,subsjson){
 		$.ajax({
			url:'/WorkSystem/manage/task/dotaskadd',	
			type:'POST',
			beforeSend:function(){
				$.AMUI.progress.start();
			},
			complete:function(){
				 $.AMUI.progress.done();
			},
			async:false,
			data:{
				title:title,
				time:time,
				body:body,
				uids:uidsjson,
				subids:subsjson,
				},
			error:function(jqXHR,textStatus,errorThrown){
				if(textStatus=="error"){
					layer.msg('内部服务错误！请联系网站管理员');
				}
			},
			success:function(data,status){
				if(data==1){
					layer.msg("添加成功")
					location.reload();
				}
				else{
					layer.msg('添加失败');
				}
			}
		});
 	}
    //加载添加关注人窗口的函数
   function loadaddpokermanwindow(){
	   $("#addpokerman").click(function(){		   
		   var html=$("#addpokermanwindow").css("display","block");
		   layer.open({
			    type: 1,
			    skin: 'layui-layer-rim', //加上边框
			    area: ['620px', '440px'], //宽高
			    content: html,
			    btn:['确认'],
			    yes:function(index,layero){
			    	 $("#pokermans").empty();
					 var pokermans=$("#chooseafter").html();
				     $("#pokermans").append(pokermans);
				     layer.close(index);
			    },
			});
		   
		   
	   });
   }  
    
 function addtimeevent(){
	 $.fn.datetimepicker.dates['zh-CN'] = {
			    days: ["星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六", "星期日"],
			    daysShort: ["周日", "周一", "周二", "周三", "周四", "周五", "周六", "周日"],
			    daysMin:  ["日", "一", "二", "三", "四", "五", "六", "日"],
			    months: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
			    monthsShort: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
			    today: "今日",
			    suffix: [],
			    meridiem: ["上午", "下午"]
			  };

			  $('.form-datetime-lang').datetimepicker({
			    language:  'zh-CN',
			    format: 'yyyy-mm-dd hh:ii'
			  });
	 
	 
 }   
    //单击每个人时，将数据从before转移到after中
   function clickadd(index){
	   //如果当前元素在after中的话，将其转移到before元素中
	  if($(index).parent().attr("id")=="chooseafter"){
		  $(index).css("position","absolute");
		  //删除添加
		   $(index).animate({
		  	  top:'-=150px',
	 		  },1000,function(){
	 	   $(index).attr("style","");
		   $(index).appendTo($("#choosebefore .content"));
	 		  });
		  return false;
	   }
	  else{
		  $(index).css("position","absolute");
		  //添加到关注的里面
		  var text=$("#chooseafter").text();
		  if($.trim(text)=="无"){
			  var cf=$("#chooseafter .am-cf").clone();
			  $("#chooseafter ").empty();
		  }
		  else{
			  var cf=$("#chooseafter .am-cf").clone();
		  }
		  $(index).animate({
		  	  top:'+=150px',
	   },1000,function(){
		   $("#chooseafter .am-cf").remove();
		   $(index).attr("style","");
		   $(index).appendTo($("#chooseafter"));
		   $("#chooseafter").append(cf);
	   });
	  }   
   }