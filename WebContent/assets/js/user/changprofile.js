function save(){
	var name=$("#user-name").val();
	var oldpwd=$("#user-oldpwd").val();
	var newpwd=$("#user-newpwd").val();
	var againpwd=$("#user-againpwd").val();
	if(name==""){
		layer.msg('姓名不能为空');
		return false;
	}
	if(oldpwd==""){
		layer.msg('旧密码不能为空');
		return false;
	}
	if(newpwd=="" || againpwd==""){
		layer.msg('新密码不能为空');
		return false;
	}
	if( newpwd!=againpwd){
		layer.msg('两次密码不匹配');
		return false;
	}
	$.ajax({
		url:'/WorkSystem/user/dochangeprofile',	
		beforeSend:function(){
			$.AMUI.progress.start();
		},
		complete:function(){
			 $.AMUI.progress.done();
		},
		async:false,
		type:"POST",
		data:{
			name:name,
			oldpwd:oldpwd,
			newpwd:newpwd,
			againpwd:againpwd
			},
		error:function(jqXHR,textStatus,errorThrown){
			if(textStatus=="error"){
				layer.msg('内部服务错误！请联系网站管理员');
			}
		},
		success:function(data,status){
			if(data>=100){
				if(data==108){
					layer.msg("原密码不正确");
				}
				else if(data==109){
					layer.msg("新密码两次不匹配")
				}
			}
			else{
				if(data==1){
					layer.msg("个人资料修改成功");
					location.reload();
				}
				else{
					layer.msg("个人资料修改失败");
				}
			}
		}
	});
}