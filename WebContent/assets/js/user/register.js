function loaddepartmentandclass(){
	$.ajax({
		url:'/WorkSystem/user/ajax/getDepartments',	
		async:false,
		type:"POST",
		dataType:"json",
		error:function(jqXHR,textStatus,errorThrown){
			if(textStatus=="error"){
				layer.msg('内部服务错误！请联系网站管理员');
			}
		},
		success:function(data,status){
			if(data!="false"){
				var departments=data.department;
				$.each(departments,function(index,department){
					$(".department").append('<option value="'+department['did']+'">'+department['name']+'</option>');
				});
				var c=data.classes;
				$.each(c,function(index,cs){
					$(".classes").append('<option value="'+cs['cid']+'">'+cs['name']+'</option>');
				});
			}
			else{
				layer.msg('请求失败');
			}
		}
	});
}

$(document).ready(function () {
	
	
	//加载默认的部门和班级
	loaddepartmentandclass();
	
	
	
	//增加注册监听事件
	addregisterevent();
	
 });

function addregisterevent(){
	//学生注册监听
	$("#studentdoregister").click(function(){
		var loginId = $("#LoginId").val();
		var loginPwd = $("#LoginPwd").val();
		var name = $("#name").val();
		var age = $("#age").val();
		if($.trim(loginId)==""){
			layer.tips('登陆名不能为空', '#LoginId');
			return false;
		}
		if($.trim(loginPwd)==""){
			layer.tips("密码不能为空",'#LoginPwd');
			return false;
		}
		if($.trim(name)==""){
			layer.tips("姓名不能为空","#name");
			return false;
		}
		if($.trim(age)==""){
			layer.tips("年龄不能为空","#age");
			return false;
		}
		$.ajax({
			url:'/WorkSystem/user/doregisterstudent',	
			beforeSend:function(){
				$.AMUI.progress.start();
			},
			complete:function(){
				 $.AMUI.progress.done();
			},
			//async:false,
			type:"POST",
			data:{
				loginId:$("#LoginId").val(),
				loginPwd:$("#LoginPwd").val(),
				name:$("#name").val(),
				age:$("#age").val(),
				department:$("#department").val(),
				classes:$("#classes").val(),
				},
			error:function(jqXHR,textStatus,errorThrown){
				if(textStatus=="error"){
					layer.msg('内部服务错误！请联系网站管理员');
				}
			},
			success:function(data,status){
				if(data>=100){
					if(data==100){
						layer.msg("用户已注册，不能重复注册");
					}
				}
				else{
					if(data==1){
						layer.msg("注册成功，即将跳转页面");
						//delay(5);
						//location.href="/WorkSystem";
					}
					else{
						layer.msg("注册失败 未知错误");
					}
				}
			}
		});
	});
	
	//教师注册监听
	$("#teacherdoregister").click(function(){
		$("#teacherdoregister").ajaxStart(function(){
			alert("kaishi");
			$.AMUI.progress.start();
		});
		$("#teacherdoregister").ajaxStop(function(){
			$.AMUI.progress.done();
		});
		
		$.ajax({
			url:'/WorkSystem/user/doregisterteacher',	
			async:false,
			type:"POST",
			beforeSend:function(){
				$.AMUI.progress.start();
			},
			complete:function(){
				 $.AMUI.progress.done();
			},
			data:{
				loginId:$("#teaLoginId").val(),
				loginPwd:$("#teaLoginPwd").val(),
				name:$("#teaname").val(),
				age:$("#teaage").val(),
				department:$("#teadepartment").val(),
				classes:$("#teaclasses").val(),
				teatitle:$("#teatitle").val(),
				},
			error:function(jqXHR,textStatus,errorThrown){
				if(textStatus=="error"){
					layer.msg('内部服务错误！请联系网站管理员');
				}
			},
			success:function(data,status){
				if(data>=100){
					if(data==100){
						layer.msg("用户已注册，不能重复注册");
					}
				}
				else{
					if(data==1){
						layer.msg("注册成功，即将跳转页面");
						setTimeout(function(){
							location.href='/WorkSystem';
						}, 1000);
					}
					else{
						layer.msg("注册失败 未知错误");
					}
				}
			}
		});
	});
}