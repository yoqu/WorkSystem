/*
Navicat MySQL Data Transfer

Source Server         : xampp
Source Server Version : 50626
Source Host           : localhost:3306
Source Database       : worksystem

Target Server Type    : MYSQL
Target Server Version : 50626
File Encoding         : 65001

Date: 2015-12-23 11:45:16
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `answer`
-- ----------------------------
DROP TABLE IF EXISTS `answer`;
CREATE TABLE `answer` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TESTid` int(11) DEFAULT NULL,
  `CONTENT` varchar(255) DEFAULT NULL,
  `sid` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_qsc4433erhbdiwenijcuamnd5` (`TESTid`),
  KEY `FK_qsc4433erhbdiwenijcuamnd5a` (`sid`),
  CONSTRAINT `FK_qsc4433erhbdiwenijcuamnd5` FOREIGN KEY (`TESTid`) REFERENCES `test` (`ID`),
  CONSTRAINT `FK_qsc4433erhbdiwenijcuamnd5a` FOREIGN KEY (`sid`) REFERENCES `subject` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of answer
-- ----------------------------
INSERT INTO `answer` VALUES ('1', '1', '选A', '1');
INSERT INTO `answer` VALUES ('4', '1', '选C', '1');
INSERT INTO `answer` VALUES ('5', '1', '选D', '1');
INSERT INTO `answer` VALUES ('6', '1', '选B', '1');
INSERT INTO `answer` VALUES ('7', '2', 'A选', '1');
INSERT INTO `answer` VALUES ('8', '2', 'B选', '1');
INSERT INTO `answer` VALUES ('9', '2', 'C选', '1');
INSERT INTO `answer` VALUES ('10', '2', 'D选', '1');
INSERT INTO `answer` VALUES ('11', '3', '1', '1');
INSERT INTO `answer` VALUES ('12', '3', '2', '1');
INSERT INTO `answer` VALUES ('13', '3', '3', '1');
INSERT INTO `answer` VALUES ('14', '3', '4', '1');
INSERT INTO `answer` VALUES ('23', '8', '4', '3');
INSERT INTO `answer` VALUES ('24', '8', '2', '3');
INSERT INTO `answer` VALUES ('25', '8', '10', '3');
INSERT INTO `answer` VALUES ('26', '8', '1', '3');

-- ----------------------------
-- Table structure for `classes`
-- ----------------------------
DROP TABLE IF EXISTS `classes`;
CREATE TABLE `classes` (
  `CID` int(11) NOT NULL AUTO_INCREMENT,
  `DID` int(11) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`CID`),
  KEY `FK_p78kmrdpv2kmufj2eg5kkfobf` (`DID`),
  CONSTRAINT `FK_p78kmrdpv2kmufj2eg5kkfobf` FOREIGN KEY (`DID`) REFERENCES `department` (`DID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of classes
-- ----------------------------
INSERT INTO `classes` VALUES ('1', '1', '软件技术');
INSERT INTO `classes` VALUES ('2', '1', '图形图像');

-- ----------------------------
-- Table structure for `comment`
-- ----------------------------
DROP TABLE IF EXISTS `comment`;
CREATE TABLE `comment` (
  `CID` int(11) NOT NULL AUTO_INCREMENT,
  `UID` int(11) DEFAULT NULL,
  `TID` int(11) DEFAULT NULL,
  `BODY` longtext,
  `TIME` datetime DEFAULT NULL,
  PRIMARY KEY (`CID`),
  KEY `FK_iptcgh34280evfoly55nnagfw` (`UID`),
  KEY `FK_7xcfl88vle1g3evicob06h9j1` (`TID`),
  CONSTRAINT `FK_7xcfl88vle1g3evicob06h9j1` FOREIGN KEY (`TID`) REFERENCES `task` (`TID`),
  CONSTRAINT `FK_iptcgh34280evfoly55nnagfw` FOREIGN KEY (`UID`) REFERENCES `user` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of comment
-- ----------------------------
INSERT INTO `comment` VALUES ('1', '16', '3', '<p>aaaaaaaaaa</p>', '2015-10-27 19:25:32');
INSERT INTO `comment` VALUES ('2', '1', '4', '<p>aaaaaaaaaa</p>', '2015-10-27 19:26:38');
INSERT INTO `comment` VALUES ('3', '1', '4', '<p>啊啊啊啊啊啊啊啊</p>', '2015-10-27 19:27:35');
INSERT INTO `comment` VALUES ('4', '1', '3', '<p>你好奥</p>', '2015-11-03 11:25:41');
INSERT INTO `comment` VALUES ('5', '1', '4', '<p>111</p>', '2015-11-15 13:49:16');
INSERT INTO `comment` VALUES ('6', '1', '4', '<p>wo le ge qu&nbsp;</p>', '2015-11-20 11:09:03');
INSERT INTO `comment` VALUES ('7', '1', '3', '<p>Hello World</p>', '2015-12-17 19:13:11');
INSERT INTO `comment` VALUES ('8', '1', '3', '<p>你好世界</p>', '2015-12-17 19:13:16');
INSERT INTO `comment` VALUES ('9', '1', '3', '<p>我已经完成了</p>', '2015-12-22 23:01:20');
INSERT INTO `comment` VALUES ('10', '1', '3', null, '2015-12-23 11:26:45');
INSERT INTO `comment` VALUES ('11', '1', '3', '<p>我也完成了</p>', '2015-12-23 11:34:21');

-- ----------------------------
-- Table structure for `department`
-- ----------------------------
DROP TABLE IF EXISTS `department`;
CREATE TABLE `department` (
  `DID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`DID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of department
-- ----------------------------
INSERT INTO `department` VALUES ('1', '信息工程系');
INSERT INTO `department` VALUES ('2', '人文艺术系');

-- ----------------------------
-- Table structure for `grade`
-- ----------------------------
DROP TABLE IF EXISTS `grade`;
CREATE TABLE `grade` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SUBJECTID` int(11) DEFAULT NULL,
  `STUDENTID` int(11) DEFAULT NULL,
  `SCORE` float DEFAULT NULL,
  `TIME` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_ktipewbixoe0h0648ye1hchr0` (`SUBJECTID`),
  KEY `FK_euu0rg87iwahqvrshfs7w6n6t` (`STUDENTID`),
  CONSTRAINT `FK_euu0rg87iwahqvrshfs7w6n6t` FOREIGN KEY (`STUDENTID`) REFERENCES `student` (`ID`),
  CONSTRAINT `FK_ktipewbixoe0h0648ye1hchr0` FOREIGN KEY (`SUBJECTID`) REFERENCES `subject` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of grade
-- ----------------------------
INSERT INTO `grade` VALUES ('12', '1', '1', '6', '3600');

-- ----------------------------
-- Table structure for `pokermans`
-- ----------------------------
DROP TABLE IF EXISTS `pokermans`;
CREATE TABLE `pokermans` (
  `PID` int(11) NOT NULL AUTO_INCREMENT,
  `USER` int(11) DEFAULT NULL,
  `TASK` int(11) DEFAULT NULL,
  PRIMARY KEY (`PID`),
  KEY `FK_l63lkurrsrg349pal46fkyu79` (`USER`),
  KEY `FK_4qsvlqk1yrks2ffxk3esesynh` (`TASK`),
  CONSTRAINT `FK_4qsvlqk1yrks2ffxk3esesynh` FOREIGN KEY (`TASK`) REFERENCES `task` (`TID`),
  CONSTRAINT `FK_l63lkurrsrg349pal46fkyu79` FOREIGN KEY (`USER`) REFERENCES `user` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pokermans
-- ----------------------------
INSERT INTO `pokermans` VALUES ('5', '1', '4');
INSERT INTO `pokermans` VALUES ('6', '4', '4');
INSERT INTO `pokermans` VALUES ('7', '5', '4');
INSERT INTO `pokermans` VALUES ('8', '1', '4');
INSERT INTO `pokermans` VALUES ('9', '4', '4');
INSERT INTO `pokermans` VALUES ('10', '5', '4');
INSERT INTO `pokermans` VALUES ('45', '1', '5');
INSERT INTO `pokermans` VALUES ('46', '4', '5');
INSERT INTO `pokermans` VALUES ('47', '4', '3');
INSERT INTO `pokermans` VALUES ('48', '5', '3');
INSERT INTO `pokermans` VALUES ('49', '1', '3');
INSERT INTO `pokermans` VALUES ('50', '1', '6');
INSERT INTO `pokermans` VALUES ('51', '1', '7');
INSERT INTO `pokermans` VALUES ('52', '4', '7');

-- ----------------------------
-- Table structure for `roles`
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `rid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `modules` longtext,
  PRIMARY KEY (`rid`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES ('1', 'teacher', 'testmanage,taskmanage,task');
INSERT INTO `roles` VALUES ('2', 'student', 'test,task');

-- ----------------------------
-- Table structure for `student`
-- ----------------------------
DROP TABLE IF EXISTS `student`;
CREATE TABLE `student` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DID` int(11) DEFAULT NULL,
  `CID` int(11) DEFAULT NULL,
  `UID` int(11) DEFAULT NULL,
  `AGE` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_ipm7gkg24bwtch16u8qp8hvg3` (`DID`),
  KEY `FK_cxvlbxgvmcmt14g0r1kdkehmd` (`CID`),
  CONSTRAINT `FK_cxvlbxgvmcmt14g0r1kdkehmd` FOREIGN KEY (`CID`) REFERENCES `classes` (`CID`),
  CONSTRAINT `FK_ipm7gkg24bwtch16u8qp8hvg3` FOREIGN KEY (`DID`) REFERENCES `department` (`DID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of student
-- ----------------------------
INSERT INTO `student` VALUES ('1', '1', '1', '1', '19');
INSERT INTO `student` VALUES ('2', '1', '2', '3', '19');
INSERT INTO `student` VALUES ('3', '1', '1', '4', '21');
INSERT INTO `student` VALUES ('4', '1', '1', '5', '18');
INSERT INTO `student` VALUES ('5', '1', '2', '18', '19');
INSERT INTO `student` VALUES ('6', '1', '1', '19', null);

-- ----------------------------
-- Table structure for `subject`
-- ----------------------------
DROP TABLE IF EXISTS `subject`;
CREATE TABLE `subject` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CLASSES` int(11) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `NATURE` varchar(255) DEFAULT NULL,
  `CREDIT` double DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_y5y1fv88b22unwn5bj4tvrts` (`CLASSES`),
  CONSTRAINT `FK_y5y1fv88b22unwn5bj4tvrts` FOREIGN KEY (`CLASSES`) REFERENCES `classes` (`CID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of subject
-- ----------------------------
INSERT INTO `subject` VALUES ('1', '1', 'JAVA程序设计', '6', '0');
INSERT INTO `subject` VALUES ('2', '1', '我的试卷2', null, '6');
INSERT INTO `subject` VALUES ('3', '1', '数据库设计与分析', null, '6');
INSERT INTO `subject` VALUES ('4', '1', 'C#期末考试试题', null, '3');

-- ----------------------------
-- Table structure for `task`
-- ----------------------------
DROP TABLE IF EXISTS `task`;
CREATE TABLE `task` (
  `TID` int(11) NOT NULL AUTO_INCREMENT,
  `USER` int(11) DEFAULT NULL,
  `TITLE` varchar(255) DEFAULT NULL,
  `BODY` longtext,
  `TIME` datetime DEFAULT NULL,
  `LIMITTIME` datetime DEFAULT NULL,
  `POKERMAN` int(11) DEFAULT NULL,
  `COMMENT` int(11) DEFAULT NULL,
  `STATUS` int(11) DEFAULT NULL,
  `CLASSESID` int(11) DEFAULT NULL,
  PRIMARY KEY (`TID`),
  KEY `FK_d0fw6ywl4v5ybtxebfo4qruxw` (`USER`),
  CONSTRAINT `FK_d0fw6ywl4v5ybtxebfo4qruxw` FOREIGN KEY (`USER`) REFERENCES `user` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of task
-- ----------------------------
INSERT INTO `task` VALUES ('2', '2', '第二个任务', '任务', '2015-10-10 00:00:00', '2015-10-14 00:00:00', '0', '0', '0', '1');
INSERT INTO `task` VALUES ('3', '16', '第s1个任务', '<p>任务开始发布aaaaaaaaaaaaaaaaaaaa</p>', '2015-12-16 20:22:27', '2016-03-01 05:25:00', '3', '6', '1', '1');
INSERT INTO `task` VALUES ('4', '16', '第二个任务', '<p>任务开始发布1</p>', '2015-10-27 10:05:57', '2015-10-07 10:30:00', '3', '3', '1', '1');
INSERT INTO `task` VALUES ('5', '16', '测试任务三', '<p>测试任务</p>', '2015-11-10 11:54:23', '2015-11-11 10:50:00', '2', '0', '1', '1');
INSERT INTO `task` VALUES ('6', '16', '优化', '<p>啊啊啊啊啊<br/></p>', '2015-12-16 20:35:51', '2015-12-03 05:50:00', '1', '0', '1', '1');
INSERT INTO `task` VALUES ('7', '16', '网站测试内容网站测试内容网站测试内容网站测试内容', '<p>网站测试内容</p>', '2015-12-22 09:30:24', '2015-12-01 09:45:00', '2', '0', '1', '1');

-- ----------------------------
-- Table structure for `teacher`
-- ----------------------------
DROP TABLE IF EXISTS `teacher`;
CREATE TABLE `teacher` (
  `TID` int(11) NOT NULL AUTO_INCREMENT,
  `CLASSESID` int(11) DEFAULT NULL,
  `AGE` int(11) DEFAULT NULL,
  `UID` int(11) DEFAULT NULL,
  `DEPARTMENTID` int(11) DEFAULT NULL,
  `TEATITLE` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`TID`),
  KEY `FK_eub3y2toely06b81363md1up8` (`CLASSESID`),
  KEY `FK_1ea0cgkbfjm8d0bfl64inm091` (`DEPARTMENTID`),
  CONSTRAINT `FK_1ea0cgkbfjm8d0bfl64inm091` FOREIGN KEY (`DEPARTMENTID`) REFERENCES `department` (`DID`),
  CONSTRAINT `FK_eub3y2toely06b81363md1up8` FOREIGN KEY (`CLASSESID`) REFERENCES `classes` (`CID`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of teacher
-- ----------------------------
INSERT INTO `teacher` VALUES ('11', '1', '25', '16', '1', '无');
INSERT INTO `teacher` VALUES ('12', '1', '46', '17', '1', '无');

-- ----------------------------
-- Table structure for `test`
-- ----------------------------
DROP TABLE IF EXISTS `test`;
CREATE TABLE `test` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SID` int(11) DEFAULT NULL,
  `CONTENT` varchar(255) DEFAULT NULL,
  `GRADE` int(11) DEFAULT NULL,
  `ANSWERID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_3x5tx08ym66ulrn2jnqyigl8b` (`SID`),
  CONSTRAINT `FK_3x5tx08ym66ulrn2jnqyigl8b` FOREIGN KEY (`SID`) REFERENCES `subject` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of test
-- ----------------------------
INSERT INTO `test` VALUES ('1', '1', '第一道题', '3', '1');
INSERT INTO `test` VALUES ('2', '1', '第二题', '3', '7');
INSERT INTO `test` VALUES ('3', '1', '1+1等于几', '3', '12');
INSERT INTO `test` VALUES ('8', '3', '3+1', '3', '23');

-- ----------------------------
-- Table structure for `tos`
-- ----------------------------
DROP TABLE IF EXISTS `tos`;
CREATE TABLE `tos` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SID` int(11) DEFAULT NULL,
  `TID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_rv6vc4ae1eqgljrmlwq8b2tj7` (`SID`),
  KEY `FK_kbsyfcrcmuutv25i3y9i9lwbr` (`TID`),
  CONSTRAINT `FK_kbsyfcrcmuutv25i3y9i9lwbr` FOREIGN KEY (`TID`) REFERENCES `task` (`TID`),
  CONSTRAINT `FK_rv6vc4ae1eqgljrmlwq8b2tj7` FOREIGN KEY (`SID`) REFERENCES `subject` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tos
-- ----------------------------
INSERT INTO `tos` VALUES ('1', '1', '3');
INSERT INTO `tos` VALUES ('3', '1', '5');
INSERT INTO `tos` VALUES ('4', '1', '6');
INSERT INTO `tos` VALUES ('5', '1', '7');
INSERT INTO `tos` VALUES ('6', '2', '7');

-- ----------------------------
-- Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `loginId` varchar(255) DEFAULT NULL,
  `loginPwd` varchar(255) DEFAULT NULL,
  `role` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`uid`),
  KEY `rolewaijian` (`role`),
  CONSTRAINT `rolewaijian` FOREIGN KEY (`role`) REFERENCES `roles` (`rid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', '小天', 'yoqu', '123456', '2', '2015-10-12 11:11:46');
INSERT INTO `user` VALUES ('2', '老师', 'ls', '123456', '1', '2015-10-11 00:00:00');
INSERT INTO `user` VALUES ('3', '临走', 'aaa', '123', '1', '2015-10-16 10:58:52');
INSERT INTO `user` VALUES ('4', '林晓川', 'yoqulin', '123456', '1', '2015-10-16 11:18:51');
INSERT INTO `user` VALUES ('5', '你好', 'yoqu', '1234567', '1', '2015-10-16 11:57:04');
INSERT INTO `user` VALUES ('16', '王五', 'ls1', '123456', '1', '2015-10-23 10:18:33');
INSERT INTO `user` VALUES ('17', '张三', 'bac', '123456', '1', '2015-10-23 10:21:10');
INSERT INTO `user` VALUES ('18', '小帅', 'shuai', '', '2', '2015-12-23 11:15:09');
INSERT INTO `user` VALUES ('19', '', '', '123456', '2', '2015-12-23 11:22:56');
